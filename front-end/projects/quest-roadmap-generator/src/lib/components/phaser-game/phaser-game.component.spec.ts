import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'projects/quest-roadmap-generator/src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Direction, Interaction } from '../../core';
import { ComponentSettings, Game, Status } from '../../models';
import { QuestRoadmapGeneratorService, GameMapDataService, InteractionsService } from '../../services';

import { PhaserGameComponent } from './phaser-game.component';

describe('PhaserGameComponent', () => {
  let component: PhaserGameComponent;
  let fixture: ComponentFixture<PhaserGameComponent>;
  
  let questRoadmapGeneratorService: QuestRoadmapGeneratorService;
  let gameMapDataService: GameMapDataService;
  let interactionsService: InteractionsService;

  let type: string = 'linear';
  let direction: string = 'right';
  let width: number = 1000;
  let height: number = 1000;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PhaserGameComponent
      ],
      imports:[
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireModule
      ],
      providers: [
        QuestRoadmapGeneratorService,
        GameMapDataService,
        InteractionsService
      ]
    })
    .compileComponents();

    questRoadmapGeneratorService = TestBed.inject(QuestRoadmapGeneratorService);
    gameMapDataService = TestBed.inject(GameMapDataService);
    interactionsService = TestBed.inject(InteractionsService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhaserGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    const gameToReturn = getGameWith2CheckPoint();
    component.gameUpdateSubject = new BehaviorSubject<Game>(gameToReturn);

    expect(component).toBeTruthy();
  });

  function getGameWith2CheckPoint() {
    const game: Game = {
        id: 1,
        createdDate: new Date(),
        description: 'un jeu',
        title: 'un titre',
        status: Status.running,
        quests: [
          {
            id: 1,
            createdDate: new Date(),
            description: 'un jeu',
            title: 'un titre',
            status: Status.todo,
            goals: [
              {
                id: 1,
                createdDate: new Date(),
                description: 'un jeu',
                title: 'un titre',
                status: Status.running,
                winnedXP: 100
              }
            ]
          },
          {
            id: 2,
            createdDate: new Date(),
            description: 'un jeu',
            title: 'un titre',
            status: Status.todo,
            goals: [
              {
                id: 1,
                createdDate: new Date(),
                description: 'un jeu',
                title: 'un titre',
                status: Status.locked,
                winnedXP: 100
              }
            ]
          }
        ]
    };

    return game;
  }
});
