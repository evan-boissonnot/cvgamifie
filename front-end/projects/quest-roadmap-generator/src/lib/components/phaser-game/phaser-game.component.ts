import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { path } from 'd3-path';
import Phaser from 'phaser';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Direction } from '../../core/direction/direction';
import { Interaction } from '../../core/interaction/interaction';
import { Map } from '../../core/map/map';
import { Game } from '../../models/game';
import { GameMapDataService } from '../../services/game-mapdata/game-mapdata.service';
import { InteractionsService } from '../../services/interactions/interactions.service';
import { QuestRoadmapGeneratorService } from '../../services/road-map/quest-roadmap-generator.service';

@Component({
  selector: 'phaser-game',
  templateUrl: './phaser-game.component.html',
  styleUrls: ['./phaser-game.component.css']
})
export class PhaserGameComponent implements OnInit {
  map: Map;

  private subscriptions: Subscription[] = [];
  @Input() gameUpdateSubject: BehaviorSubject<Game>;
  @Input() validatorSubject: BehaviorSubject<Interaction[][]>;

  @Input() type: string;
  @Input() direction: string;
  @Input() width: number;
  @Input() height: number;

  @Output() mapDataEmitter = new EventEmitter<any[]>();

  @Output() validateEmitter = this._interactionsService.validateEmitter;
  @Output() gotoEmitter = this._interactionsService.gotoEmitter;
  @Output() updateEmitter = this._interactionsService.updateEmitter;
  @Output() finishEmitter = this._interactionsService.finishEmitter;

  successCallback = () => {
    this.successMessage = 'Map generated';
  };
  errorCallback = () => {
    this.errorMessage = 'Error, please setup minimum 2 checkpoints';
  };

  successMessage = '';
  errorMessage = '';

  game: Game;
  lastUpdate: number;

  phaserGame: Phaser.Game;
  config: Phaser.Types.Core.GameConfig;

  constructor(private _service: QuestRoadmapGeneratorService, private _gameMapDataService: GameMapDataService, private _interactionsService: InteractionsService) {

  }

  //#region Public methods
  ngOnDestroy(): void {
    this.subscriptions.forEach(item => item.unsubscribe());
  }

  ngOnInit(): void {
    const gameUpdateSubscription = this.gameUpdateSubject.subscribe(item => {
      this.game = item;

      if (!this.map) {
        this._service.getMap(this.game, this.successCallback, this.errorCallback, this.initComponentsSettings()).subscribe(item => {
          this.game = item.game;
          this.map = item.map;

          this.map.styles.backgroundColor = 8421504;

          this.mapDataEmitter.emit(this.map.components);

          this.config = {
            type: Phaser.AUTO,
            width: this.width,
            height: this.height,
            parent: 'gameContainer',
            scene: this.map,
            physics: {
              default: 'arcade',
              arcade: {
                gravity: { y: 100 }
              }
            },
            backgroundColor: 8421504,
          };

          this.phaserGame = new Phaser.Game(this.config);
        });
      } else {
        this._service.updateMap(this.game, this.map).subscribe(item => {
          this.game = item.game;
          this.map.components = item.components;
          this.map.options = item.settings;

          this.mapDataEmitter.emit(this.map.components);

          this.map.updateMap();
        });
      }
    });

    if (this.validatorSubject) {
      const validatorSubscription = this.validatorSubject.subscribe(item => {
        this._interactionsService.validatorsSubject.next(item);
      });

      this.addSubsciption(validatorSubscription);
    }

    this.addSubsciption(gameUpdateSubscription);
  }
  //#endregion

  //#region Internal methods
  private addSubsciption(subscription): void {
    this.subscriptions.push(subscription);
  }

  private initComponentsSettings(): any {
    let direction = ({
      "top": Direction.top,
      "bottom": Direction.bottom,
      "left": Direction.left
    }[this.direction]) || Direction.right;

    const componentSettings = {
      pathType: this.type,
      direction: direction,
      width: this.width,
      height: this.height
    };

    return componentSettings;
  }
}