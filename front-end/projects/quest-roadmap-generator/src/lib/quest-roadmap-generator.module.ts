import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { environment } from '../environments/environment';
import { PhaserGameComponent } from './components/phaser-game/phaser-game.component';
import { GameMapDataService } from './services/game-mapdata/game-mapdata.service';
import { InteractionsService } from './services/interactions/interactions.service';
import { QuestRoadmapGeneratorService } from './services/road-map/quest-roadmap-generator.service';

@NgModule({
  declarations: [
    PhaserGameComponent
  ],
  imports: [
    CommonModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule
  ],
  providers: [
    GameMapDataService,
    QuestRoadmapGeneratorService,
    InteractionsService
  ],
  exports: [
    PhaserGameComponent
  ]
})
export class QuestRoadmapGeneratorModule { }
