import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Interaction } from '../../core/interaction/interaction';
import { InteractionType } from '../../models/interaction-type';

@Injectable()
export class InteractionsService {
    public validatorsSubject: BehaviorSubject<Interaction[][]> = new BehaviorSubject<Interaction[][]>(null);

    validateEmitter = new EventEmitter<any>();
    gotoEmitter = new EventEmitter<any>();
    updateEmitter = new EventEmitter<any>();
    finishEmitter = new EventEmitter<any>();

    constructor() { }

    interact(type, checkpointType, checkpointId, parentCheckpointId?) {
        if (type == InteractionType.validate) {
            this.validateEmitter.emit({
                type: type,
                checkpointType: checkpointType,
                id: checkpointId
            });
        }

        if (type == InteractionType.goto) {
            this.gotoEmitter.emit({
                type: type,
                checkpointType: checkpointType,
                id: checkpointId,
                parentId: parentCheckpointId
            });
        }

        if (type == InteractionType.update) {
            this.updateEmitter.emit({
                type: type,
                checkpointType: checkpointType,
                id: checkpointId
            });
        }

        if (type == InteractionType.finish) {
            this.finishEmitter.emit({
                type: type
            });
        }
    }
}
