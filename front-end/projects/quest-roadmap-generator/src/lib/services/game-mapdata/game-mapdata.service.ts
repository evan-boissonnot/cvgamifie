import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/compat/database';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Game } from '../../models/game';

@Injectable()
export class GameMapDataService {
  mapsRef: AngularFireList<any>;
  maps: Observable<any[]>;

  constructor(private _db: AngularFireDatabase) {
    this.mapsRef = this._db.list("/maps");
    this.maps = this.mapsRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
  }

  getOneById(game_id: number): Observable<any> {
    return this.mapsRef.valueChanges().pipe(
      map(map => {
        return map.find(el => el.game_id == game_id);
      }));
  }

  getAll(): Observable<any> {
    return this.mapsRef.valueChanges();
  }

  addOne(game: Game, data: any, date: number): void {
    this.getOneById(game.id).subscribe(map => {
      if (!map) {
        this.mapsRef.push({ game_id: game.id, map_data: data, last_update: date });
      }
    })
  }

  updateOneById(game_id: number, data: any, last_update: number): void {
    this.maps.subscribe(map => {
      const founded = map.find(el => el.game_id == game_id);
      let msg = 'Map up to date';

      if(founded.last_update > last_update){
        msg = ' > Map not up to date';
      }

      if(founded.last_update < last_update){
        msg = 'error';
      }
    });
  }

  updateOne(key: string, game: Game, data: any, date: number): void {
    this.mapsRef.update(key, { game_id: game.id, map_data: data, last_update: date });
  }
  deleteOne(key: string): void {
    this.mapsRef.remove(key);
  }
  deleteAll(): void {
    this.mapsRef.remove();
  }
}
