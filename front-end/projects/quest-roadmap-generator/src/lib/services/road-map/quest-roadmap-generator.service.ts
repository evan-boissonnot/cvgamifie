import { Injectable } from '@angular/core';
import MapSettings from '../../core/map-settings';
import { Observable, of } from 'rxjs';
import { RoadMap } from '../../models/road-map';
import { Game } from '../../models/game';
import { GameMapDataService } from '../game-mapdata/game-mapdata.service';
import { Status } from '../../models/status';
import { ComponentSettings } from '../../models/components-settings';
import { map } from 'rxjs/operators';
import { Map } from '../../core/map/map';
import { InteractionsService } from '../interactions/interactions.service';
import { MapUpdate } from '../../models/update';

/**
 * Quest RoadMap generator that permit to generate roadmaps from html element
 */
@Injectable()
export class QuestRoadmapGeneratorService {
  constructor(private _firebaseService: GameMapDataService, private _interactionsService: InteractionsService) { }

  //#region Public methods
  /**
   * Creates the map, from game object
   * Be careful : not generate the map in the template
   * @param {Game} game
   * @param {Function} successCallback
   * @param {Function} errorCallback
   * @param {componentSettings} componentSettings
   */
  public getMap(game: Game, successCallback: Function, errorCallback: Function, componentSettings: ComponentSettings): Observable<RoadMap> {
    let settings = this.initComponentMapSettings(componentSettings);

    settings.successCallback = successCallback;
    settings.errorCallback = errorCallback;

    return this._firebaseService.getOneById(game.id).pipe(
      map(map => {
        if (map) {
          settings.data = map.map_data;
        } else {
          let checkpoints = [];

          checkpoints.push({ type: 'start', status: Status.finished });

          game.quests.forEach(quest => {
            let checkpoint = { label: quest.title, type: null, childrens: null, status: quest.status };

            if (quest.goals) {
              let goals = [];

              quest.goals.forEach(goal => {
                goals.push({ label: goal.title, type: 'children', status: goal.status });
              });

              checkpoint.childrens = goals;
            }

            checkpoints.push(checkpoint);
          })

          checkpoints.push({ type: 'end', status: Status.locked });

          settings.data = checkpoints;
        }

        return {
          game: game,
          map: new Map(settings, this._interactionsService),
          settings: settings
        }
      })
    );
  }

  public updateMap(game: Game, map: Map): Observable<MapUpdate> {
    let updated_map = map;
    let settings = updated_map.options;

    const playerPositionTopOffset = 80;

    updated_map.components.forEach((quest, quest_id) => {
      if (quest_id != 0 && quest_id <= game.quests.length) {
        quest.label = game.quests[quest_id - 1].title;
        quest.status = game.quests[quest_id - 1].status;

        if (quest.status == Status.running) {
          updated_map.path.playerPosition = { x: quest.x, y: quest.y - playerPositionTopOffset };
        }

        if (quest.childrens) {
          quest.childrens.forEach((goal, goal_id) => {
            goal.label = game.quests[quest_id - 1].goals[goal_id].title;
            goal.status = game.quests[quest_id - 1].goals[goal_id].status;

            if (goal.status == Status.running) {
              updated_map.path.playerPosition = { x: goal.x, y: goal.y - playerPositionTopOffset };
            }
          });
        }
      }
    });

    return of({
      game: game,
      components: updated_map.components,
      settings: settings
    })
  }
  //#endregion

  //#region Internal methods
  private initComponentMapSettings(componentSettings: ComponentSettings): any {
    let settings = MapSettings;

    settings.pathType = componentSettings.pathType;
    settings.direction = componentSettings.direction;

    settings.sizing = {
      width: settings.width - (settings.margin.right + settings.margin.left),
      height: settings.height - (settings.margin.top + settings.margin.bottom)
    }

    if (componentSettings.width) {
      settings.width = componentSettings.width;
      settings.sizing.width = componentSettings.width - (settings.margin.right + settings.margin.left);
    }

    if (componentSettings.height) {
      settings.height = componentSettings.height;
      settings.sizing.height = componentSettings.height - (settings.margin.top + settings.margin.bottom);
    }

    return settings;
  }
  //#endregion
}
