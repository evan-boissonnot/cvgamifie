import { async, TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire';
import { Map } from '@dev-to-be-curious/rand-svg-path-generator/dist';
import { environment } from 'projects/quest-roadmap-generator/src/environments/environment';
import { Direction } from '../../core';
import { ComponentSettings, Game, Status } from '../../models';
import { QuestRoadmapGeneratorService, GameMapDataService, InteractionsService } from '../../services';

describe('QuestRoadmapGeneratorService', () => {
  let service: QuestRoadmapGeneratorService;
  let gameMapDataService: GameMapDataService;
  let interactionsService: InteractionsService;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireModule
      ],
      providers: [
        QuestRoadmapGeneratorService,
        GameMapDataService,
        InteractionsService
      ]
    });

    service = TestBed.inject(QuestRoadmapGeneratorService);
    gameMapDataService = TestBed.inject(GameMapDataService);
    interactionsService = TestBed.inject(InteractionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a new map from rand-svg-path-generator', () => {
    const gameToReturn = getGameWith1CheckPoint();
    const componentSettings = getComponentSettings("linear");

    const settings = {
      successCallBack: () => {
      },
      errorCallBack: () => {
      }
    };

    let roadmap;
    
    service.getMap(gameToReturn, settings.successCallBack, settings.errorCallBack, componentSettings).subscribe(roadmap => {
      roadmap = roadmap
    });

    expect(roadmap).toBeTruthy();
    expect(roadmap.game).toBeTruthy();
    expect(roadmap.map).toBeTruthy();
    expect(roadmap.map).toBeInstanceOf(Map);
    expect(roadmap.settings).toBeTruthy();
  });

  it('should get success callback when generate map', () => {
    const gameToReturn = getGameWith2CheckPoint();
    const componentSettings = getComponentSettings("linear");

    const settings = {
      successCallBack: () => {
      },
      errorCallBack: () => {
      }
    };

    const spySuccess = spyOn(settings, 'successCallBack');

    service.getMap(gameToReturn, spySuccess, settings.errorCallBack, componentSettings).subscribe(roadmap => {
      // roadmap.map.generateMap();
      expect(roadmap.map).toBeTruthy();
    });

    expect(spySuccess).toHaveBeenCalled();
  });

  it('should get error callback when generate map', () => {
    const gameToReturn = getGameWith1CheckPoint();
    const componentSettings = getComponentSettings("linear");

    const settings = {
      successCallBack: () => {
      },
      errorCallBack: () => {
      }
    };

    const spyError = spyOn(settings, 'errorCallBack');

    service.getMap(gameToReturn, settings.successCallBack, spyError, componentSettings).subscribe(roadmap => {
      // roadmap.map.generateMap();
      expect(roadmap.map).toBeTruthy();
    });

    expect(spyError).toHaveBeenCalled();
  });

  function getComponentSettings(type: "linear" | "random"): ComponentSettings {
    const componentSettings = {
      pathType: type,
      direction: Direction.right,
      width: 1000,
      height: 1000
    }
    return componentSettings;
  }

  function getGameWith1CheckPoint() {
    const game: Game = {
      id: 1,
      createdDate: new Date(),
      description: 'un jeu',
      title: 'un titre',
      status: Status.running,
      quests: [
        {
          id: 1,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.todo,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.running,
              winnedXP: 100
            }
          ]
        }
      ]
    };

    return game;
  }

  function getGameWith2CheckPoint() {
    const game: Game = {
      id: 1,
      createdDate: new Date(),
      description: 'un jeu',
      title: 'un titre',
      status: Status.running,
      quests: [
        {
          id: 1,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.todo,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.running,
              winnedXP: 100
            }
          ]
        },
        {
          id: 2,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.todo,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.locked,
              winnedXP: 100
            }
          ]
        }
      ]
    };

    return game;
  }
});

