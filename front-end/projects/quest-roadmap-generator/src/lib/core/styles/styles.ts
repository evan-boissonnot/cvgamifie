import { InteractionType } from "../../models/interaction-type";

export interface Styles {
    backgroundColor: number | string;

    path: {
        fill: number,
        stroke: string,
        strokeAlpha: number,
        strokeWidth: number
    };

    checkpoints: {
        default: {
            fill: number,
            stroke: number,
            strokeWidth: number,
            strokeDasharray: string,
            strokeAlpha: number,
            label: {
                fill: string,
                font: string,
                fontWeight: string
            }
        },
        start: {
            fill: number,
            stroke: number,
            strokeWidth: number,
            strokeDasharray: string,
            strokeAlpha: number,
            label: {
                fill: string,
                font: string,
                fontWeight: string
            }
        },
        end: {
            fill: number,
            stroke: number,
            strokeWidth: number,
            strokeDasharray: string,
            strokeAlpha: number,
            label: {
                fill: string,
                font: string,
                fontWeight: string
            }
        },
        children: {
            fill: number,
            stroke: number,
            strokeWidth: number,
            strokeDasharray: string,
            strokeAlpha: number,
            label: {
                fill: string,
                font: string,
                fontWeight: string
            }
        }
    };

    status: {
        locked: {
            circle: {
                fill: number,
                opacity: number
            },
            title: {
                fill: string,
                opacity: number
            },
            assets: {
                icon: {
                    url: string
                }
            }
        },
        todo: {
            circle: {
                fill: number,
                opacity: number
            },
            title: {
                fill: string,
                opacity: number
            },
            assets: {
                icon: {
                    url: string
                }
            }
        },
        running: {
            circle: {
                fill: number,
                opacity: number
            },
            title: {
                fill: string,
                opacity: number
            },
            assets: {
                icon: {
                    url: string
                },
            }
        },
        paused: {
            circle: {
                fill: number,
                opacity: number
            },
            title: {
                fill: string,
                opacity: number
            },
            assets: {
                icon: {
                    url: string
                },
            }
        },
        finished: {
            circle: {
                fill: number,
                opacity: number
            },
            title: {
                fill: string,
                opacity: number
            },
            assets: {
                icon: {
                    url: string
                }
            }
        }
    };

    interactions: {
        validate: {
            backgroundColor: string,
            stroke: number,
            strokeWidth: number,
            strokeAlpha: number,
            label: {
                color: string,
                font: string,
                fontWeight: string
            },
            hover: {
                backgroundColor: string,
                label: {
                    color: string,
                }
            }
        },
        goto: {
            backgroundColor: string,
            stroke: number,
            strokeWidth: number,
            strokeAlpha: number,
            label: {
                color: string,
                font: string,
                fontWeight: string
            },
            hover: {
                backgroundColor: string,
                label: {
                    color: string,
                }
            }
        },
        update: {
            backgroundColor: string,
            stroke: number,
            strokeWidth: number,
            strokeAlpha: number,
            label: {
                color: string,
                font: string,
                fontWeight: string
            },
            hover: {
                backgroundColor: string,
                label: {
                    color: string,
                }
            }
        },
        finish: {
            backgroundColor: string,
            stroke: number,
            strokeWidth: number,
            strokeAlpha: number,
            label: {
                color: string,
                font: string,
                fontWeight: string
            },
            hover: {
                backgroundColor: string,
                label: {
                    color: string,
                }
            }
        }
    };

    player: {
        assets: {
            icon: {
                url: string,
                iconSize: {
                    x: number,
                    y: number
                }
            }
        }
    };
}
