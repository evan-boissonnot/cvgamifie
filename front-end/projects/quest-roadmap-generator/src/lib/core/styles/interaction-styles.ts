export interface InteractrionStyles {
    backgroundColor: string,
    stroke: number,
    strokeWidth: number,
    strokeAlpha: number,
    label: {
        color: string,
        font: string,
        fontWeight: string
    },
    hover: {
        backgroundColor: string,
        label: {
            color: string,
        }
    }
}
