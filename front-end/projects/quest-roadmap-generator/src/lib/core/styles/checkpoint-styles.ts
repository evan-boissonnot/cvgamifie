export interface CheckpointStyles {
    fill: number,
    stroke: number,
    strokeWidth: number,
    strokeDasharray: string,
    strokeAlpha: number,
    label: {
        fill: string,
        font: string,
        fontWeight: string
    },
    status: {
        circle: {
            fill: number,
            opacity: number
        },
        title: {
            fill: string,
            opacity: number
        },
        assets: {
            icon: {
                url: string;
            };
        }
    }
}
