import { Styles } from "./styles";

const styles: Styles = {
    backgroundColor: 8421504,
    path: {
        fill: 2631720,
        stroke: "grey",
        strokeWidth: 4,
        strokeAlpha: 1
    },
    checkpoints: {
        default: {
            fill: 4620980,
            stroke: 11119017,
            strokeWidth: 2,
            strokeDasharray: "none",
            strokeAlpha: 1,
            label: {
                fill: "#4B0082",
                font: "16px sans-serif",
                fontWeight: "bold"
            }
        },
        start: {
            fill: 2096671,
            stroke: 0,
            strokeWidth: 2,
            strokeDasharray: "3",
            strokeAlpha: 1,
            label: {
                fill: "#4B0082",
                font: "18px sans-serif",
                fontWeight: "bold"
            }
        },
        end: {
            fill: 15737608,
            stroke: 0,
            strokeWidth: 2,
            strokeDasharray: "3",
            strokeAlpha: 1,
            label: {
                fill: "#4B0082",
                font: "16px sans-serif",
                fontWeight: "bold"
            }
        },
        children: {
            fill: 15765789,
            stroke: 0,
            strokeWidth: 2,
            strokeDasharray: "3",
            strokeAlpha: 1,
            label: {
                fill: "#4B0082",
                font: "16px sans-serif",
                fontWeight: "bold"
            }
        }
    },
    status: {
        locked: {
            circle: {
                fill: 3880251,
                opacity: 0.5
            },
            title: {
                fill: "#2E2E2E",
                opacity: 0.8
            },
            assets: {
                icon: {
                    url: ""
                }
            }
        },
        todo: {
            circle: {
                fill: 4620980,
                opacity: 1
            },
            title: {
                fill: "#4B0082",
                opacity: 1
            },
            assets: {
                icon: {
                    url: "../../../assets/@dev-to-be-curious/todo.svg"
                }
            }
        },
        running: {
            circle: {
                fill: 4620980,
                opacity: 1
            },
            title: {
                fill: "#4B0082",
                opacity: 1
            },
            assets: {
                icon: {
                    url: "../../../assets/@dev-to-be-curious/running.svg"
                },
            }
        },
        paused: {
            circle: {
                fill: 4620980,
                opacity: 1
            },
            title: {
                fill: "#4B0082",
                opacity: 1
            },
            assets: {
                icon: {
                    url: "../../../assets/@dev-to-be-curious/paused.svg"
                },
            }
        },
        finished: {
            circle: {
                fill: 7792652,
                opacity: 1
            },
            title: {
                fill: "#14C201",
                opacity: 1
            },
            assets: {
                icon: { 
                    url: "../../../assets/@dev-to-be-curious/finished.svg"
                }
            }
        }
    },
    interactions: {
        goto: {
            backgroundColor: "#111",
            stroke: 11119017,
            strokeWidth: 2,
            strokeAlpha: 1,
            label: {
                color: "white",
                font: "12px sans-serif",
                fontWeight: "bold"
            },
            hover: {
                backgroundColor: "white",
                label: {
                    color: "black",
                }
            }
        },
        validate: {
            backgroundColor: "#111",
            stroke: 0,
            strokeWidth: 2,
            strokeAlpha: 1,
            label: {
                color: "white",
                font: "12px sans-serif",
                fontWeight: "bold"
            },
            hover: {
                backgroundColor: "white",
                label: {
                    color: "black",
                }
            }
        },
        finish: {
            backgroundColor: "#111",
            stroke: 0,
            strokeWidth: 2,
            strokeAlpha: 1,
            label: {
                color: "white",
                font: "12px sans-serif",
                fontWeight: "bold"
            },
            hover: {
                backgroundColor: "white",
                label: {
                    color: "black",
                }
            }
        },
        update: {
            backgroundColor: "#111",
            stroke: 0,
            strokeWidth: 2,
            strokeAlpha: 1,
            label: {
                color: "white",
                font: "12px sans-serif",
                fontWeight: "bold"
            },
            hover: {
                backgroundColor: "white",
                label: {
                    color: "black",
                }
            }
        }
    },
    player: {
        assets: {
            icon: {
                url: "../../../assets/@dev-to-be-curious/player.svg",
                iconSize: {
                    x: 40,
                    y: 40
                }
            }
        }
    },
};

export default styles;
