import { Status } from "../../models/status";
import { MapOption } from "../map/map-options";
import { PathPointOption } from "../path/path-point-option";
import { CheckpointStyles } from "../styles/checkpoint-styles";
import { Styles } from "../styles/styles";
import styles from "../styles/styles-options";
import Phaser from 'phaser';
import { Position } from "../position/position";

/**
 * CheckPoint class permit to generate Checkpoints
 */
export class CheckPoint extends Phaser.GameObjects.Container {
    options: MapOption;
    checkpointType: string;
    status: Status;
    position: any;
    label: any;
    radius: number;
    styles: CheckpointStyles;

    checkpointStyles: Styles['checkpoints'] = styles.checkpoints;

    /**
     * Instanciate a new Checkpoint on a rand-svg-path-generator Path
     * @param {Phaser.Scene} scene
     * @param {MapOption} options
     * @param {PathPointOption} position
     * format: { x: .., y: .. } | default: random
     * @param {string} label
     * default: Quest
     * @param {string} type
     * default: quest
     * @param {Status} status
     * default: TODO
     */
    constructor(scene: Phaser.Scene, options: MapOption, position: PathPointOption, label: string = "Quest", type: string = "quest", status: Status = Status.todo) {
        super(scene);

        this.scene.add.existing(this);

        this.defineSettings(options, position, label, type, status);

        this.setStyle();
    }

    /**
     * Draw checkpoint
     */
    draw() {
        let color = ({
            "start": styles.checkpoints.start.fill,
            "end": styles.checkpoints.end.fill,
            "children": styles.checkpoints.children.fill
        })[this.checkpointType] || styles.checkpoints.default.fill;

        let checkpoint: Phaser.GameObjects.Arc = this.scene.add.circle(this.position.x, this.position.y, this.radius, color);
        checkpoint.setStrokeStyle(this.styles.strokeWidth, this.styles.stroke, this.styles.strokeAlpha);

        let textStyle = {
            color: this.styles.label.fill,
            font: this.styles.label.font,
            FontStyle: this.styles.label.fontWeight
        }

        let title: Phaser.GameObjects.Text = this.scene.add.text(this.position.x, this.position.y - 40, this.label.text).setColor(textStyle.color)
            .setFont(textStyle.font)
            .setFontStyle(textStyle.FontStyle);

        let bounds = title.getBounds();
        title.setX(bounds.x - (bounds.width / 2));

        this.add([checkpoint, title]);

        this.generateStatusIcon(checkpoint, title, { x: 25, y: 25 });
    }

    /**
     * Update checkpoint
     */
    updateCheckpoint(options: MapOption, position: PathPointOption, label: string, type: string, status: Status) {
        this.removeAll(true);

        this.defineSettings(options, position, label, type, status);

        this.setStyle();

        this.draw();
    }

    /**
     * Generate checkpoints status icons from on a rand-svg-path-generator Path
     * @param {any} group
     * Root element of the checkpoint
     * @param {any} circle
     * Circle element
     * @param {any} title
     * Title element
     * @param {CheckPoint} previousPoint
     */
    private generateStatusIcon(checkpoint: Phaser.GameObjects.Arc, title: Phaser.GameObjects.Text, iconSize: { x: number, y: number }): void {
        let icon: Phaser.GameObjects.Image;

        let position: Position = {
            x: this.position.x + (this.radius / 2),
            y: this.position.y + (this.radius / 2)
        }

        let texture = ({
            "LOCKED": () => {
                let overlay: Phaser.GameObjects.Arc = this.scene.add.circle(this.position.x, this.position.y, this.radius, this.styles.status.circle.fill, this.styles.status.circle.opacity);
                overlay.setStrokeStyle(this.styles.strokeWidth, this.styles.stroke, this.styles.strokeAlpha);

                title.setColor(this.styles.status.title.fill);
                this.add(overlay);
            },
            "RUNNING": () => { return 'running-icon' },
            "PAUSED": () => {
                checkpoint.fillColor = this.styles.status.circle.fill;
                title.setFill(this.styles.status.title.fill);

                return 'paused-icon';
            },
            "FINISHED": () => {
                checkpoint.fillColor = this.styles.status.circle.fill;
                title.setFill(this.styles.status.title.fill);

                return 'finished-icon';
            }
        })[this.status] || (() => { return 'todo-icon' });

        if (this.status != Status.locked) {
            icon = this.scene.add.image(position.x, position.y, texture());
            icon.setDisplaySize(iconSize.x, iconSize.y);

            this.add(icon);
        } else {
            texture();
        }
    }

    private setStyle(): void {
        let customCheckpointType = ({
            "start": this.checkpointStyles.start,
            "end": this.checkpointStyles.end,
            "children": this.checkpointStyles.children
        })[this.checkpointType] || this.checkpointStyles.default;

        // Get types styles
        const statusStyles: Styles['status'] = styles.status;

        let checkpointStatus = ({
            "LOCKED": statusStyles.locked,
            "RUNNING": statusStyles.running,
            "PAUSED": statusStyles.paused,
            "FINISHED": statusStyles.finished
        })[this.status] || statusStyles.todo;

        this.styles = {
            fill: customCheckpointType.fill,
            stroke: customCheckpointType.stroke,
            strokeWidth: customCheckpointType.strokeWidth,
            strokeDasharray: customCheckpointType.strokeDasharray,
            strokeAlpha: customCheckpointType.strokeAlpha,
            label: {
                fill: customCheckpointType.label.fill,
                font: customCheckpointType.label.font,
                fontWeight: customCheckpointType.label.fontWeight
            },
            status: {
                circle: {
                    fill: checkpointStatus.circle.fill,
                    opacity: checkpointStatus.circle.opacity
                },
                title: {
                    fill: checkpointStatus.title.fill,
                    opacity: checkpointStatus.title.opacity
                },
                assets: checkpointStatus.assets
            }
        };
    }

    private defineSettings(options: MapOption, position: PathPointOption, label: string, type: string, status: Status): void {
        this.options = options;

        this.checkpointType = type;

        this.status = status;

        this.position = {
            x: position.x,
            y: position.y
        };

        this.label = {
            text: label
        }

        let radiusType = ({
            "children": this.options.checkpoints.radius / 1.25
        })[this.checkpointType] || this.options.checkpoints.radius;

        this.radius = radiusType;
    }
}
