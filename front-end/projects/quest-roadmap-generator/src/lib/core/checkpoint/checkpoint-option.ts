import { Status } from "../../models/status";

/**
 * Options to set a chekpoints
 */
export interface CheckPointOption {
    /**
     * Name of the checkpoint
     */
    label: string;

    /**
     * Type of the checkpoint: start | quest | children | end
     */
    type: string;

    /**
     * X position of the checkpoint 
     */
    x: number;

    /**
     * Y position of the checkpoint 
     */
    y: number;

    /**
     * List of children's checkpoints
     */
    childrens: Array<CheckPointOption>;

    /**
     * Current status of the checkpoint: LOCKED | TODO | RUNNING | PAUSED | FINISHED
     */
    status: Status
}
