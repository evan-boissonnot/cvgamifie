/**
 * Path Sizing properties
 */
 export interface PathSizing {
    width: number;
    height: number;
}
