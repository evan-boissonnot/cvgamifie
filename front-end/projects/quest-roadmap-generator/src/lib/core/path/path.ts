import { Status } from "../../models/status";
import { CheckPointOption } from "../checkpoint/checkpoint-option";
import { ErrorType } from "../errors/error-type";
import { MapOption } from "../map/map-options";
import { Styles } from "../styles/styles";
import styles from "../styles/styles-options";
import { PathPointOption } from "./path-point-option";
import Phaser from 'phaser';
import { Player } from "../player/player";
import { CheckPoint } from "../checkpoint/checkpoint";
import { PathSizing } from "./path-sizing";
/**
 * Path class that permit to generate path and checkpoints
 */
export abstract class Path extends Phaser.GameObjects.Container {
    options: MapOption;
    start: CheckPointOption;
    end: CheckPointOption;
    points: any[];
    checkpoints: CheckPoint[] = [];
    styles: Styles['path'];
    radius: number;

    player: Player;
    playerPosition: PathPointOption;

    path: Phaser.Curves.Path;

    constructor(scene: Phaser.Scene, options: MapOption) {
        super(scene);

        this.scene.add.existing(this);

        const checkpointStrokeWidth = 8;

        this.options = options;

        this.radius = this.options.checkpoints.radius + checkpointStrokeWidth;

        this.styles = styles.path;
    }

    abstract init(): void;

    abstract preload(): void;

    abstract create(): void;

    /**
     * Generate a Path on a rand-svg-path-generator Map
     */
    generatePath(): void {
        let data = this.options.data;
        this.points = data;

        if (data.length >= 2) {
            let checkpoints = [];

            this.generatePathPoints();

            this.points.forEach(checkpoint => {
                checkpoints.push(checkpoint)

                if (this.getChildrenCount(checkpoint)) {
                    checkpoint.childrens.forEach(children => {
                        checkpoints.push(children);
                    });
                }
            });

            this.points = checkpoints;

            this.generateCheckPoints();
            this.create();

            if (this.playerPosition) {
                this.player = new Player(this.scene, this.playerPosition);
                this.player.draw();
            }

            this.setGameSize();

            this.options?.successCallback?.call(this);
        } else {
            let errorMessage: string = 'Please setup 2 quests at least';

            let text = this.scene.add.text(((this.options.width - (errorMessage.length * 3.75)) / 2), ((this.options.height - (errorMessage.length * 3.75)) / 2), errorMessage, { fontFamily: 'Georgia, "Goudy Bookletter 1911", Times, serif', color: 'red' });
            this.add(text);

            this.options?.errorCallback?.call(this, { name: ErrorType.NotEnoughCheckPoints.toString(), message: errorMessage, type: ErrorType.NotEnoughCheckPoints });
        }
    }

    updatePath(): void {
        this.updateCheckpoints(this.points);

        this.player.updatePlayer(this.playerPosition);

        this.setGameSize();
    }

    /**
     * Generate Path data on a rand-svg-path-generator Map
     */
    generatePathPoints(): void {
        this.doGeneratePathPoints();
    }

    /**
     * Generate Path data on a rand-svg-path-generator Map (Overidable method)
     */
    protected abstract doGeneratePathPoints(): void;

    /**
     * Define start point properties
     */
    defineStart(label: string = "Start", type: string = "start", x: number = 0, y: number = 0, childrens: CheckPointOption[] = [], status: Status = Status.finished): void {
        this.start = {
            label: label,
            type: type,
            x: x,
            y: y,
            childrens: childrens,
            status: status
        };
    }

    /**
     * Define end point properties
     */
    defineEnd(label: string = "End", type: string = "end", x: number = null, y: number = null, childrens: CheckPointOption[] = [], status: Status = Status.locked): void {
        this.end = {
            label: label,
            type: type,
            x: x,
            y: y,
            childrens: childrens,
            status: status
        };
    }

    /**
     * Generate x and y positions for a path point 
     * @param {CheckPointOption} point
     * @param {CheckPointOption} previousPoint
     * @param {any} options
     * options for random generation - (min | max | deltaY | alpha { minAlpha | max_alpha } | distance)
     * @returns {CheckPointOption} point
     */
    abstract generatePointPosition(point: CheckPointOption, previousPoint: CheckPointOption, options: any): CheckPointOption;

    /**
     * Generate checkpoints from Path data on a rand-svg-path-generator Path
     */
    generateCheckPoints(): void {
        this.points.map((checkpoint) => {
            let checkpointLabel = checkpoint.label

            if (!checkpoint.label) {
                checkpointLabel = ({
                    "start": checkpoint.label = this.start.label,
                    "end": checkpoint.label = this.end.label
                })[checkpoint.type] || checkpoint.label;
            }

            checkpoint.label = checkpointLabel;
        });
    }

    updateCheckpoints(points): void {
        this.checkpoints.forEach((checkpoint, id) => {
            checkpoint.updateCheckpoint(this.options, { x: points[id].x, y: points[id].y }, points[id].label, points[id].type, points[id].status);
        });
    }

    /**
     * Get total checkpoints count ( without goals )
     * @param {Array<CheckPointOption>} checkpoints
     * 
     * @returns {number} count
     */
    getCheckpointsCount(checkpoints: Array<CheckPointOption>): number {
        return checkpoints.length;
    }

    /**
     * Get total childrens count of a checkpoint
     * @param {CheckPointOption} checkpoints
     * 
     * @returns {number} count
     */
    getChildrenCount(checkpoint: CheckPointOption): number {
        let count = 0;
        if (checkpoint.childrens) {
            count += checkpoint.childrens.length;
        }
        return count;
    }

    /**
     * Get total childrens count of all checkpoints
     * @param {Array<CheckPointOption>} checkpoints
     * 
     * @returns {number} count
     */
    getTotalChildrenCount(checkpoints: Array<CheckPointOption>): number {
        let count = 0;
        checkpoints.forEach(checkpoint => {
            count += this.getChildrenCount(checkpoint);
        });
        return count;
    }

    /**
     * Return the path length in pixels from starting point to last
     */
    getPathLength(): PathSizing {
        let pathLength = ({
            "bottom": {
                width: this.options.width,
                height: this.points[this.points.length - 1].y - this.points[0].y
            }
        }[this.options.direction]) || {
            width: this.points[this.points.length - 1].x - this.points[0].x,
            height: this.options.height
        };

        return pathLength;
    }

    setGameSize(): void {
        const pathLenght = this.getPathLength();

        this.scene.game.scale.setGameSize(pathLenght.width, pathLenght.height);
    }
}
