/**
 * Options to set the callbacks
 */
 export interface CallBackOption {
    /**
     * Will be called when build is done
     */
    successCallback?: Function;

    /**
     * Will be called if there is any error duration path building
     */
    errorCallback?: Function;
}
