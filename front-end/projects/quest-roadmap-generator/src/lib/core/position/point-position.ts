import { CheckPointOption } from "../checkpoint/checkpoint-option";

export interface PointPosition {

    /**
     * Verify min and max y positions
     */
    createOne();

    /**
     * Generate x & y positions for a point
     * @param {CheckPointOption} point
     * @param {CheckPointOption} previousPoint
     * @param {any} options
     * 
     * @returns {CheckPointOption} point
     */
    generatePointPosition(point: CheckPointOption, previousPoint: CheckPointOption, options: any): CheckPointOption;
}
