import { Direction } from "../../direction/direction";
import { PointPosition } from "../point-position";
import { PointPositionStrategy } from "./point-position-strategy";

export class PointPositionBuilder {
    private strategy: PointPositionStrategy;

    constructor(private direction: Direction) {
        this.strategy = new PointPositionStrategy(); 
    }

    /**
     * Return a PointPosition according to the direction strategy
     */
    build(): PointPosition {
        return this.strategy.createOne(this.direction);
    }
}
