import { CheckPointOption } from "../../checkpoint/checkpoint-option";
import { PointPosition } from "../point-position";


export class TopPointPosition implements PointPosition{

    constructor() { }

    createOne() { }

    /**
     * Generate x & y positions for a point
     * @param {CheckPointOption} point
     * @param {CheckPointOption} previousPoint
     * @param {any} options
     * 
     * @returns {CheckPointOption} point
     */
    generatePointPosition(point: CheckPointOption, previousPoint: CheckPointOption, options: any): CheckPointOption {
        if (!point.x) {
            point.x = previousPoint.x;
        }
    
        if (!point.y) {
            point.y = previousPoint.y - options.distance;
        }

        return point;
    }
}
