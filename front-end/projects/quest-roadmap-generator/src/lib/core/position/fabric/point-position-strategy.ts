import { Direction } from "../../direction/direction";
import { BottomPointPosition } from "./bottom-point-position";
import { LeftPointPosition } from "./left-point-position";
import { PointPosition } from "../point-position";
import { RightPointPosition } from "./right-point-position";
import { TopPointPosition } from "./top-point-position";

export class PointPositionStrategy {
    private types = {
        "top": TopPointPosition,
        "bottom": BottomPointPosition,
        "right": RightPointPosition,
        "left": LeftPointPosition
    };
    
    constructor() { }
    
    /**
     * Return a PointPosition according to the direction
     * @param {Direction} direction
     * Direction of the path: top | bottom | right | left
     */
    createOne(direction: Direction): PointPosition{
        let pointPosition: PointPosition = new this.types[direction]();

        return pointPosition;
    }
}
