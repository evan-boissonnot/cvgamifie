import { EventEmitter } from '@angular/core';
import Phaser from 'phaser';
import { InteractionType } from "../../models/interaction-type";
import { InteractionsService } from '../../services/interactions/interactions.service';
import { Position } from "../position/position";
import { InteractrionStyles } from "../styles/interaction-styles";
import { Styles } from "../styles/styles";
import styles from "../styles/styles-options";
import { Interaction } from './interaction';

/**
 * CheckPoint class permit to generate Checkpoints
 */
export class Validator extends Phaser.GameObjects.Container {

    type: InteractionType;
    position: any;

    validateEmitter = new EventEmitter<any>();
    gotoEmitter = new EventEmitter<any>();
    updateEmitter = new EventEmitter<any>();
    finishEmitter = new EventEmitter<any>();

    interaction: Interaction;

    public label: string;
    styles: InteractrionStyles;

    /**
     * Instanciate a new Checkpoint on a rand-svg-path-generator Path
     * @param {Phaser.Scene} scene
     * A Phaser scene
     * @param {Position} position
     * format: { x: .., y: .. } | default: random
     * @param {string} label
     * default: Quest
     */
    constructor(scene: Phaser.Scene, type: InteractionType, position: Position, interaction: Interaction, private _interactionsService: InteractionsService) {
        super(scene);

        this.scene.add.existing(this);

        this.type = type;

        this.position = {
            x: position.x,
            y: position.y
        };

        this.interaction = {
            type: type,
            checkpointId: interaction.checkpointId,
            parentCheckpointId: interaction.parentCheckpointId,
            checkpointType: interaction.checkpointType
        }

        this.label = ({
            "validate": 'Valider',
            "goto": 'Aller à',
            "update": 'Mettre à jour',
            "finish": 'Terminer'
        })[this.type] || 'default';

        // Get types styles
        const interactionsStyles: Styles['interactions'] = styles.interactions;

        let interactionStyles = ({
            "validate": interactionsStyles.validate,
            "goto": interactionsStyles.goto,
            "update": interactionsStyles.update,
            "finish": interactionsStyles.finish
        })[this.type] || interactionsStyles.goto;

        this.styles = {
            backgroundColor: interactionStyles.backgroundColor,
            stroke: interactionStyles.stroke,
            strokeWidth: interactionStyles.strokeWidth,
            strokeAlpha: interactionStyles.strokeAlpha,
            label: {
                color: interactionStyles.label.color,
                font: interactionStyles.label.font,
                fontWeight: interactionStyles.label.fontWeight
            },
            hover: {
                backgroundColor: interactionStyles.hover.backgroundColor,
                label: {
                    color: interactionStyles.hover.label.color,
                }
            }
        };
    }

    /**
     * Draw checkpoint
     */
    public draw(): void {
        let interactionButton = this.scene.add.text(this.position.x, this.position.y, this.label).setOrigin(0.5)
            .setPadding(10, 2)
            .setStyle({
                backgroundColor: this.styles.backgroundColor,
                fill: this.styles.label.color,
                font: this.styles.label.font,
                fontStyle: this.styles.label.fontWeight
            })
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => {
                this._interactionsService.interact(this.type, this.interaction.checkpointType, this.interaction.checkpointId, this.interaction.parentCheckpointId);
            })
            .on('pointerover', () => interactionButton.setStyle({
                backgroundColor: this.styles.hover.backgroundColor,
                fill: this.styles.hover.label.color
            }))
            .on('pointerout', () => interactionButton.setStyle({
                backgroundColor: this.styles.backgroundColor,
                fill: this.styles.label.color
            }));;

        this.add([interactionButton]);
    }

    /**
     * Update checkpoint
     */
    public update(): void { }
}
