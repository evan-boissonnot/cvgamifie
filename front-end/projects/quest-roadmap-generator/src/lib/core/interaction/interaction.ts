import { CheckpointType } from "../../models/checkpoint-type";
import { InteractionType } from "../../models/interaction-type";

export interface Interaction { 
    type: InteractionType;

    checkpointId: number;

    parentCheckpointId: number;

    checkpointType: CheckpointType;

}