import { MapOption } from "../../map/map-options";
import { PathSizing } from "../../path/path-sizing";

export abstract class OptionDirection {
    constructor() {}

    /**
     * Return the properties of the starting point
     * @param {PathSizing} path_sizing
     * Width and height of the checkpoint without margin
     * @param {MapOption} options
     * Checkpoints radius
     */
    abstract getStartOptions(options: MapOption);

    /**
     * Return the path length in pixels from starting point to last
     * @param {any} first_point
     * @param {any} last_point
     */
    abstract getPathLength(first_point: any, last_point: any): PathSizing;
}
