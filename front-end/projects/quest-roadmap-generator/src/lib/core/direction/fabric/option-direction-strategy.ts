import { Direction } from "../direction";
import { BottomOptionDirection } from "./bottom-option-direction";
import { LeftOptionDirection } from "./left-option-direction";
import { OptionDirection } from "./option-direction";
import { RightOptionDirection } from "./right-option-direction";
import { TopOptionDirection } from "./top-option-direction";

export class OptionDirectionStrategy {
    private types = {
        "top": TopOptionDirection,
        "bottom": BottomOptionDirection,
        "right": RightOptionDirection,
        "left": LeftOptionDirection
    };

    constructor() { }
    
    /**
     * Return an OptionDirection according to the direction
     * @param {Direction} direction
     * Direction of the path: top | bottom | right | left
     */
    createOne(direction: Direction): OptionDirection{
        let optionDirection: OptionDirection = new this.types[direction]();

        return optionDirection;
    }
}
