import { EVENT_MANAGER_PLUGINS } from "@angular/platform-browser";
import { Status } from "../../../models/status";
import { MapOption } from "../../map/map-options";
import { PathSizing } from "../../path/path-sizing";
import { OptionDirection } from "./option-direction";

export class BottomOptionDirection extends OptionDirection {
    constructor() {
        super();
    }

    /**
     * Return the properties of the starting point
     * @param {MapOption} options
     * Checkpoints radius
     */
    getStartOptions(options: MapOption) {
        const position = {
            x: options.width / 2,
            y: options.margin.top + (options.checkpoints.radius * 2)
        };

        const startOption = { label: "Begin", type: "start", isRandom: false, x: position.x, y: position.y, childrens: [], status: Status.finished };

        return startOption;
    }

    /**
     * Return the path length in pixels from starting point to last
     * @param {any} first_point
     * @param {any} last_point
     */
    getPathLength(first_point: any, last_point: any): PathSizing {
        const pathLength = {
            width: last_point.x - first_point.x,
            height: last_point.y - first_point.y
        };

        return pathLength;
    }
}
