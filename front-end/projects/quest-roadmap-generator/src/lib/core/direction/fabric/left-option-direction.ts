import { OptionDirection } from "./option-direction";
import { PathSizing } from "../../path/path-sizing";
import { Status } from "../../../models/status";
import { MapOption } from "../../map/map-options";

export class LeftOptionDirection extends OptionDirection {
    constructor() {
        super();
    }

    /**
     * Return the properties of the starting point
     * @param {MapOption} options
     * Checkpoints radius
     */
    getStartOptions(options: MapOption) {
        const position = {
            x: options.width - (options.checkpoints.radius * 2),
            y: (options.height / 2) + options.checkpoints.radius
        };

        const startOption = { label: "Begin", type: "start", isRandom: false, x: position.x, y: position.y, childrens: [], status: Status.finished };

        return startOption;
    }

    /**
     * Return the path length in pixels from starting point to last
     * @param {any} first_point
     * @param {any} last_point
     */
    getPathLength(first_point: any, last_point: any): PathSizing {
        const pathLength = {
            width: first_point.x - last_point.x,
            height: last_point.y - first_point.y
        };

        return pathLength;
    }
}
