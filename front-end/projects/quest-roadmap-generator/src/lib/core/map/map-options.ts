import { CallBackOption } from "../callback-option";
import { Direction } from "../direction/direction";

/**
 * Option to configure the map
 */
export interface MapOption extends CallBackOption {
    
    /**
     * Data to generate map
     */
    data: any;

    /**
     * Width of the map background
     */
    width: number;

    /**
     * Height of the map background
     */
    height: number;

    /**
     * Margin of the map
     */
    margin: { top: number, bottom: number, right: number, left: number };

    /**
     * Size properties of the map without margin
     */
    sizing: { width: number, height: number };

    /**
     * Option for checkpoint configuration
     */
    checkpoints: {
        radius: number,
        randomizer: { maxAlpha: number, distance: number }
    };
    
    /**
     * Path type: random | linear
     */
    pathType: "random" | "linear";
    
    /**
     * Path directions
     */
    direction: Direction;
}
