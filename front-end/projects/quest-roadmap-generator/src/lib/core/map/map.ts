import { InteractionsService } from "../../services/interactions/interactions.service";
import { CheckPointOption } from "../checkpoint/checkpoint-option";
import { Interaction } from "../interaction/interaction";
import { Validator } from "../interaction/validator";
import { PathFabric } from "../path/fabric/path-fabric";
import { Path } from "../path/path";
import { Styles } from "../styles/styles";
import styles from "../styles/styles-options";
import { MapOption } from "./map-options";

/**
 * Map class that permit to generate svg canvas with a Path
 */
export class Map extends Phaser.Scene {
    options: MapOption;
    components: any;
    start: CheckPointOption;
    end: CheckPointOption;
    path: Path;
    styles: Styles;

    validators: Validator[] = [];

    /**
     * Instanciate a new Map from rand-svg-path-generator
     * @param {MapOption} options
     * default: canvas
     */
    constructor(options: MapOption, private _interactionsService: InteractionsService) {
        super({ key: 'map' });

        this.options = options;

        this.components = this.options.data;

        if (!this.options.width) {
            this.options.width = innerWidth * .75;
        }

        if (!this.options.height) {
            this.options.height = innerHeight * .75;
        }

        this.styles = styles;
    }

    init(/*params*/): void {

    }

    preload(): void {
        // this.load.svg('locked-icon', styles.status.locked.assets.icon.url);
        this.load.svg('running-icon', styles.status.running.assets.icon.url);
        this.load.svg('paused-icon', styles.status.paused.assets.icon.url);
        this.load.svg('finished-icon', styles.status.finished.assets.icon.url);
        this.load.svg('todo-icon', styles.status.todo.assets.icon.url);

        this.load.svg('player-icon', styles.player.assets.icon.url);
    }

    create(): void {
        this.path = PathFabric.createPath(this, this.options.pathType, this.options);
        this.path.generatePath();

        let positions = this.path.points.map(checkpoint => {
            return { x: checkpoint.x, y: checkpoint.y };
        });

        if (this.validators) {
            this.initValidators(positions);
        }

        this.initCamera();
    }

    update(time): void {

    }

    public updateMap(): void {
        this.path.updatePath();

        if (this.validators) {
            this.validators.forEach(button => {
                button.destroy(true);
            });

            this.validators = [];

            let positions = this.path.points.map(checkpoint => {
                return { x: checkpoint.x, y: checkpoint.y };
            });

            this.initValidators(positions);
        }
    }

    private initCamera(): void {
        let cam = this.cameras.main;

        let sizing = ({
            "top": {
                width: this.options.width,
                height: this.path.getPathLength().height + this.options.margin.top + this.options.margin.bottom
            },
            "bottom": {
                width: this.options.width,
                height: this.path.getPathLength().height + this.options.margin.top + (this.options.margin.bottom * 2)
            },
            "left": {
                width: this.path.getPathLength().width + this.options.margin.left + this.options.margin.right,
                height: this.options.height
            }
        }[this.options.direction]) || {
            width: this.path.getPathLength().width + this.options.margin.left + this.options.margin.right,
            height: this.options.height
        };

        cam.setBounds(0, 0, sizing.width, sizing.height);

        this.input.on('pointermove', p => {
            if (!p.isDown) return;

            cam.scrollX -= (p.x - p.prevPosition.x);
            cam.scrollY -= (p.y - p.prevPosition.y);
        });
    }

    private initValidators(positions): void {
        this._interactionsService.validatorsSubject.subscribe(item => {
            if(item) {
                positions.forEach((position, id) => {
                    let validators = item[id];
    
                    validators?.forEach((validator, id) => {
                        const pos = {
                            x: position.x + (this.options.checkpoints.radius * 4) + (75 * id),
                            y: position.y
                        }
    
                        let interaction: Interaction = {
                            type: validator.type,
                            checkpointId: validator.checkpointId,
                            parentCheckpointId: validator.parentCheckpointId,
                            checkpointType: validator.checkpointType
                        }
    
                        let button = new Validator(this, validator.type, pos, interaction, this._interactionsService);
                        button.draw();
    
                        this.validators.push(button);
                    });
                });
            }
        });
    }
}