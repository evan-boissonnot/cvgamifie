/**
 * One goal of one quest, from a Game
 */
 export enum InteractionType {
    validate = "validate",
    goto = "goto",
    update = "update",
    finish = "finish"
}