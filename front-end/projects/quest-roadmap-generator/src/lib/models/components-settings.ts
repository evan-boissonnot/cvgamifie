import { Direction } from "../core/direction/direction";

/**
 * RoadMap component settings
 */
export interface ComponentSettings {
    /** 
     * Type of the path
     * */
    pathType: "random" | "linear";
    
    /** 
     * Direction of the path
     * */
    direction: Direction;
    
    /** 
     * Width of the component  
     * */
    width: number;
    
    /** 
     * Heigth of the component 
     * */
    height: number;
    
}
