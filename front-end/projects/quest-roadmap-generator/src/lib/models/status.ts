/**
 * Option to configure the map
 */
export enum Status {
    locked = "LOCKED",
    todo = "TODO",
    running = "RUNNING",
    paused = "PAUSED",
    finished = "FINISHED"
}