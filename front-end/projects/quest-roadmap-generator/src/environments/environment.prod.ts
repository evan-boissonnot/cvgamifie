export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCPTEvJlRmWmdL2_bK3uTeq7wGHNM22oy0",
    authDomain: "cvgamifie.firebaseapp.com",
    databaseURL: "https://cvgamifie-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "cvgamifie",
    storageBucket: "cvgamifie.appspot.com",
    messagingSenderId: "683449453525",
    appId: "1:683449453525:web:119c5de9e8658d6cf98b5c",
    measurementId: "G-5585MEPLGR"
  }
};
