import { TestBed } from '@angular/core/testing';
import { LoggerService } from './logger.service';


describe('LoggerService', () => {
  let service: LoggerService;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        LoggerService
      ]
    });
    service = TestBed.inject(LoggerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
