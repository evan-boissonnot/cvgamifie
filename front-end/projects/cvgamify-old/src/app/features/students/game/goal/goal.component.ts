import { Component, OnInit, Input } from '@angular/core';
import { Goal } from '../../../../core/models/goal';

@Component({
  selector: 'cvgamifie-goal',
  templateUrl: './goal.component.html',
  styleUrls: ['./goal.component.sass']
})
export class GoalComponent implements OnInit {
  @Input()
  goal: Goal;

  constructor() { }

  ngOnInit(): void {
  }

}
