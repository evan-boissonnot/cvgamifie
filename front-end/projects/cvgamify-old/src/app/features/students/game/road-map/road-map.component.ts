import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Quest } from 'projects/cvgamify-old/src/app/core/models/quest';
import { GameModel } from 'projects/cvgamify-old/src/app/core/models/signal-r/game-model';
import { GoalModel } from 'projects/cvgamify-old/src/app/core/models/signal-r/goal-model';
import { QuestModel } from 'projects/cvgamify-old/src/app/core/models/signal-r/quest-model';
import { GameService } from 'projects/cvgamify-old/src/app/shared/services/game/game.service';
import { LoggerService } from 'projects/cvgamify-old/src/app/shared/services/logger/logger.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Game } from '../../../../core/models/game';

@Component({
  selector: 'cvgamifie-road-map',
  templateUrl: './road-map.component.html',
  styleUrls: ['./road-map.component.sass']
})
export class RoadMapComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private gameId = 0;

  public gameUpdateSubject: BehaviorSubject<Game> = this.gameService.gameUpdateSubject ;
  public game: Game;

  constructor(public gameService: GameService, private route: ActivatedRoute, private logger: LoggerService) { }

  //#region Public methods
  ngOnDestroy(): void {
    this.subscriptions.forEach(item => item.unsubscribe());
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.gameId = parseInt(params.id);
    });

    const signalRSubscription = this.gameService.startConnection().subscribe(() => {
      this.logger.info('Connection started');
    }, error => {
      this.logger.error('Error while starting connection: ' + error);
    });

    const gameSubscription = this.gameService.selectOnResume(this.gameId).subscribe(item => {
      this.gameUpdateSubject.next(item);
    });

    const gameUpdateSubscription = this.gameUpdateSubject.subscribe(item => {
      this.game = item;
    });

    const signalRBehaviorSubscription = this.gameService.gameModelBehaviorSubject.subscribe(item => {
      this.updateGame(item);
    });

    this.addSubscription(gameSubscription);
    this.addSubscription(gameUpdateSubscription);
    this.addSubscription(signalRSubscription);
    this.addSubscription(signalRBehaviorSubscription);

    this.gameService.addUpdateGameListener();
  }
  //#endregion

  //#region Internal methods
  private addSubscription(subscription): void {
    this.subscriptions.push(subscription);
  }

  private updateGame(new_game: Game): void {
    let current_game: Game = this.game;

    if(new_game){
      if(current_game.status != new_game.status){
        this.updateStatus(new_game.status, current_game);
      }

      if(current_game.title != new_game.title){
        this.updateTitle(new_game.title, current_game);
      }

      if(current_game.description != new_game.description){
        this.updateDescription(new_game.description, current_game);
      }

      current_game.title = new_game.title;
      current_game.description = new_game.description;
      current_game.status = new_game.status;

      let new_quests: Quest[] = current_game.quests.map((quest, i) => {
        if(quest.status != new_game.quests[i].status){
          this.updateStatus(new_game.quests[i].status, quest);
        }

        if(quest.title != new_game.quests[i].title){
          this.updateTitle(new_game.quests[i].title, quest);
        }

        if(quest.description != new_game.quests[i].description){
          this.updateDescription(new_game.quests[i].description, quest);
        }

        if(quest.goals){
          quest.goals.map((goal, id) => {
            if(goal.status != new_game.quests[i].goals[id].status){
              this.updateStatus(new_game.quests[i].goals[id].status, goal);
            }

            if(goal.title != new_game.quests[i].goals[id].title){
              this.updateTitle(new_game.quests[i].goals[id].title, goal);
            }

            if(goal.description != new_game.quests[i].goals[id].description){
              this.updateDescription(new_game.quests[i].goals[id].description, goal);
            }
          });
        }

        return quest
      });

      current_game.quests = new_quests;

      this.gameUpdateSubject.next(current_game);
    }
  }

  private updateStatus(status: string, object: GameModel | QuestModel | GoalModel): void {
    object.status = status;
  }

  private updateTitle(title: string, object: GameModel | QuestModel | GoalModel): void {
    object.title = title;
  }

  private updateDescription(description: string, object: GameModel | QuestModel | GoalModel): void {
    object.description = description;
  }
  //#endregion
}
