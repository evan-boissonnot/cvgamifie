import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventoryComponent } from './inventory/inventory.component';
import { RoadMapComponent } from './road-map/road-map.component';

const routes: Routes = [
  {
    path: '', component: InventoryComponent
  },  
  {
    path: 'road-map/:id', component: RoadMapComponent
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameRoutingModule { }
