import { Component, OnInit } from '@angular/core';
import { GameService } from '../../../../shared/services/game/game.service';
import { Observable } from 'rxjs';
import { Game } from '../../../../core/models/game';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.sass']
})
export class InventoryComponent implements OnInit {
  game$: Observable<Game>;

  constructor(private _service: GameService) { }

  ngOnInit(): void {
    const gameId: number = 1;
    this.game$ = this._service.selectOnResume(gameId);
  }

}
