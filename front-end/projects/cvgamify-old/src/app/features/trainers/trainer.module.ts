import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import * as SignalR from '@microsoft/signalr';
import { HubConnection } from '@microsoft/signalr';
import { environment } from 'projects/cvgamify-old/src/environments/environment';
import { QuestRoadmapGeneratorModule } from 'projects/quest-roadmap-generator/src/public-api';
import { GameService } from '../../shared/services/game/game.service';
import { HomeComponent } from './home/home.component';
import { RoadMapComponent } from './road-map/road-map.component';
import { TrainerRoutingModule } from './trainer-routing.module';

@NgModule({
  declarations: [
    HomeComponent,
    RoadMapComponent
  ],
  imports: [
    CommonModule,
    TrainerRoutingModule,
    QuestRoadmapGeneratorModule
  ],
  providers: [
    GameService,
    { provide: HubConnection, useValue: new SignalR.HubConnectionBuilder().withUrl(environment.apis.hubs.trainer.url).build() }
  ],
  exports: [
    HomeComponent
  ]
})
export class TrainerModule { }
