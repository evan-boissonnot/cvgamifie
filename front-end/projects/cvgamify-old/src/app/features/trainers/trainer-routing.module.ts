import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoadMapComponent } from './road-map/road-map.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'road-map/:id', component: RoadMapComponent
  },
  {
    path: '', component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainerRoutingModule { }
