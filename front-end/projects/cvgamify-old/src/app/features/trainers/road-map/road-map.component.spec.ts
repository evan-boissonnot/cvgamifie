import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularFireDatabase, AngularFireDatabaseModule, SnapshotAction } from '@angular/fire/database';
import { ActivatedRoute } from '@angular/router';
import * as SignalR from '@microsoft/signalr';
import { HubConnection } from '@microsoft/signalr';
import { environment } from 'projects/cvgamify-old/src/environments/environment';
import { GameMapDataService, InteractionsService, PhaserGameComponent, QuestRoadmapGeneratorService } from 'quest-roadmap-generator';
import { Observable, of } from 'rxjs';
import { Game } from '../../../core/models/game';
import { Status } from '../../../core/models/status';
import { GameService } from '../../../shared/services/game/game.service';
import { LoggerService } from '../../../shared/services/logger/logger.service';
import { RoadMapComponent } from './road-map.component';


describe('RoadMapComponent (trainer)', () => {
  let component: RoadMapComponent;
  let fixture: ComponentFixture<RoadMapComponent>;

  let httpClient: HttpClient;
  let httpMock: HttpTestingController;

  let questRoadmapGeneratorService: QuestRoadmapGeneratorService;
  let gameMapDataService: GameMapDataService;
  let gameService: GameService;

  const fakeAngularFirebase = {
    list: function(list: string) {
      return {
        snapshotChanges(): Observable<SnapshotAction<any>[]> {
          return of(this.actions)
        }
      }
    }
  };

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [
        RoadMapComponent,
        PhaserGameComponent
      ],
      schemas: [],
      imports: [
        HttpClientTestingModule,
        AngularFireDatabaseModule
      ],
      providers: [
        GameService,
        GameMapDataService,
        LoggerService,
        QuestRoadmapGeneratorService,
        InteractionsService,
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              id: getGameWith1CheckPoint().id,
            })
          }
        },
        { provide: HubConnection, useValue: new SignalR.HubConnectionBuilder().withUrl(environment.apis.hubs.trainer.url).build() },
        { provide: AngularFireDatabase, useValue: fakeAngularFirebase }
      ]
    }).compileComponents();

    httpClient = TestBed.inject(HttpClient);
    httpMock = TestBed.inject(HttpTestingController);
    gameService = TestBed.inject(GameService);
    questRoadmapGeneratorService = TestBed.inject(QuestRoadmapGeneratorService);
    gameMapDataService = TestBed.inject(GameMapDataService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have roadmap element', () => {
    component.game = getGameWith2CheckPoint();

    fixture.detectChanges();
    const compiled = fixture.nativeElement;

    expect(compiled.querySelector('phaser-game')).toBeTruthy();
  });

  it('should call the right api to get one game', () => {
    const gameToReturn = getGameWith2CheckPoint();

    let results = { param: 'id', value: gameToReturn.id };
    let url = `${environment.apis.games.url}?${results.param}=${results.value}`;

    const testRequest = httpMock.expectOne(url);
    expect(testRequest.request.method).toBe('GET');

    expect(testRequest.flush(gameToReturn));
  });

  it('should render game title', () => {
    component.game = getGameWith2CheckPoint();

    fixture.detectChanges();
    const compiled = fixture.nativeElement;

    const title = compiled.querySelector('h1#title');

    expect(title).toBeTruthy();
  });

  it('should render roadmap elements', () => {
    component.game = getGameWith2CheckPoint();

    fixture.detectChanges();
    const compiled = fixture.nativeElement;

    const title = compiled.querySelector('canvas');

    expect(title).toBeTruthy();
  });

  function getGameWith1CheckPoint() {
    const game: Game = {
      id: 5,
      createdDate: new Date(),
      title: 'Cross-group',
      description: 'Recusandae et corrupti. Saepe ut eos voluptatem. Quod ad qui nisi voluptatibus temporibus.',
      status: Status.todo,
      quests: [
        {
          id: 902,
          createdDate: new Date(),
          title: 'User-centric',
          description: 'Magni veniam facilis ut quam vel provident ut. Ipsam esse necessitatibus ullam. Rerum recusandae expedita corporis perferendis harum sed tempore. Illum vitae quaerat a rerum vitae officiis nemo tempora quia. Facere at et est. Ea quae similique.',
          status: Status.locked,
          goals: [
            {
              id: 319,
              createdDate: new Date(),
              title: 'Down-sized',
              description: 'Molestiae nemo facilis pariatur aut ut. Repellendus consequatur eum labore perferendis qui et assumenda voluptate. Vero et dolorem. Et ipsum fuga at quasi dolor.',
              status: Status.locked,
              winnedXP: 50
            },
            {
              id: 743,
              createdDate: new Date(),
              title: 'Organic',
              description: 'Et dolorem asperiores sit magnam pariatur. Harum est repudiandae id quis et deleniti. Amet vitaevoluptates sequi ipsa consequuntur cum facilis optio.',
              status: Status.locked,
              winnedXP: 50
            }
          ]
        }
      ]
    };

    return game;
  }

  function getGameWith2CheckPoint() {
    const game: Game = {
      id: 5,
      createdDate: new Date(),
      title: 'Cross-group',
      description: 'Recusandae et corrupti. Saepe ut eos voluptatem. Quod ad qui nisi voluptatibus temporibus.',
      status: Status.todo,
      quests: [
        {
          id: 902,
          createdDate: new Date(),
          title: 'User-centric',
          description: 'Magni veniam facilis ut quam vel provident ut. Ipsam esse necessitatibus ullam. Rerum recusandae expedita corporis perferendis harum sed tempore. Illum vitae quaerat a rerum vitae officiis nemo tempora quia. Facere at et est. Ea quae similique.',
          status: Status.locked,
          goals: [
            {
              id: 319,
              createdDate: new Date(),
              title: 'Down-sized',
              description: 'Molestiae nemo facilis pariatur aut ut. Repellendus consequatur eum labore perferendis qui et assumenda voluptate. Vero et dolorem. Et ipsum fuga at quasi dolor.',
              status: Status.locked,
              winnedXP: 50
            },
            {
              id: 743,
              createdDate: new Date(),
              title: 'Organic',
              description: 'Et dolorem asperiores sit magnam pariatur. Harum est repudiandae id quis et deleniti. Amet vitaevoluptates sequi ipsa consequuntur cum facilis optio.',
              status: Status.locked,
              winnedXP: 50
            }
          ]
        },
        {
          id: 818,
          createdDate: new Date(),
          title: 'Multi-channelled',
          description: 'Accusamus suscipit et libero est occaecati officia similique a. Unde ad nobis ea inventore laborum.Eligendi maiores suscipit et possimus esse est.',
          status: Status.locked,
          goals: [
            {
              id: 117,
              createdDate: new Date(),
              title: 'Balanced',
              description: 'Iste est nisi voluptas architecto ab doloremque harum. Accusamus similique aut earum. Saepe autarchitecto veniam quis a harum sint voluptas.',
              status: Status.locked,
              winnedXP: 50
            },
            {
              id: 33,
              createdDate: new Date(),
              title: 'Re-engineered',
              description: 'Dolorem et rerum perferendis hic sit quia molestias. Tempore voluptas neque quia soluta placeat.Praesentium laboriosam voluptatem fugiat odio sit atque asperiores neque. Maiores ut ipsum. Aut officiis animi eum et quia deleniti quis ipsum.',
              status: Status.locked,
              winnedXP: 50
            },
            {
              id: 659,
              createdDate: new Date(),
              title: 'Multi-channelled',
              description: 'Distinctio rem molestiae recusandae voluptatem nobis quia aut. Asperiores excepturi optio. Ut laboriosam quod dolor error maiores reprehenderit iste. Maxime nam necessitatibus quia exercitationem fuga et. Sequi voluptatem aquasi repudiandae recusandae consequatur voluptatem. Id soluta nemo reiciendis blanditiis eum voluptatem ut pariatur velit.',
              status: Status.locked,
              winnedXP: 50
            }
          ]
        }
      ]
    };

    return game;
  }

  function getGameWith3CheckPoint() {
    const game: Game = {
      id: 5,
      createdDate: new Date(),
      title: 'Cross-group',
      description: 'Recusandae et corrupti. Saepe ut eos voluptatem. Quod ad qui nisi voluptatibus temporibus.',
      status: Status.todo,
      quests: [
        {
          id: 902,
          createdDate: new Date(),
          title: 'User-centric',
          description: 'Magni veniam facilis ut quam vel provident ut. Ipsam esse necessitatibus ullam. Rerum recusandae expedita corporis perferendis harum sed tempore. Illum vitae quaerat a rerum vitae officiis nemo tempora quia. Facere at et est. Ea quae similique.',
          status: Status.locked,
          goals: [
            {
              id: 319,
              createdDate: new Date(),
              title: 'Down-sized',
              description: 'Molestiae nemo facilis pariatur aut ut. Repellendus consequatur eum labore perferendis qui et assumenda voluptate. Vero et dolorem. Et ipsum fuga at quasi dolor.',
              status: Status.locked,
              winnedXP: 50
            },
            {
              id: 743,
              createdDate: new Date(),
              title: 'Organic',
              description: 'Et dolorem asperiores sit magnam pariatur. Harum est repudiandae id quis et deleniti. Amet vitaevoluptates sequi ipsa consequuntur cum facilis optio.',
              status: Status.locked,
              winnedXP: 50
            }
          ]
        },
        {
          id: 818,
          createdDate: new Date(),
          title: 'Multi-channelled',
          description: 'Accusamus suscipit et libero est occaecati officia similique a. Unde ad nobis ea inventore laborum.Eligendi maiores suscipit et possimus esse est.',
          status: Status.locked,
          goals: [
            {
              id: 117,
              createdDate: new Date(),
              title: 'Balanced',
              description: 'Iste est nisi voluptas architecto ab doloremque harum. Accusamus similique aut earum. Saepe autarchitecto veniam quis a harum sint voluptas.',
              status: Status.locked,
              winnedXP: 50
            },
            {
              id: 33,
              createdDate: new Date(),
              title: 'Re-engineered',
              description: 'Dolorem et rerum perferendis hic sit quia molestias. Tempore voluptas neque quia soluta placeat.Praesentium laboriosam voluptatem fugiat odio sit atque asperiores neque. Maiores ut ipsum. Aut officiis animi eum et quia deleniti quis ipsum.',
              status: Status.locked,
              winnedXP: 50
            },
            {
              id: 659,
              createdDate: new Date(),
              title: 'Multi-channelled',
              description: 'Distinctio rem molestiae recusandae voluptatem nobis quia aut. Asperiores excepturi optio. Ut laboriosam quod dolor error maiores reprehenderit iste. Maxime nam necessitatibus quia exercitationem fuga et. Sequi voluptatem aquasi repudiandae recusandae consequatur voluptatem. Id soluta nemo reiciendis blanditiis eum voluptatem ut pariatur velit.',
              status: Status.locked,
              winnedXP: 50
            }
          ]
        },
        {
          id: 812,
          createdDate: new Date(),
          title: 'Multi-channelled',
          description: 'Accusamus suscipit et libero est occaecati officia similique a. Unde ad nobis ea inventore laborum.Eligendi maiores suscipit et possimus esse est.',
          status: Status.locked,
          goals: [
            {
              id: 17,
              createdDate: new Date(),
              title: 'Balanced',
              description: 'Iste est nisi voluptas architecto ab doloremque harum. Accusamus similique aut earum. Saepe autarchitecto veniam quis a harum sint voluptas.',
              status: Status.locked,
              winnedXP: 50
            },
            {
              id: 330,
              createdDate: new Date(),
              title: 'Re-engineered',
              description: 'Dolorem et rerum perferendis hic sit quia molestias. Tempore voluptas neque quia soluta placeat.Praesentium laboriosam voluptatem fugiat odio sit atque asperiores neque. Maiores ut ipsum. Aut officiis animi eum et quia deleniti quis ipsum.',
              status: Status.locked,
              winnedXP: 50
            },
            {
              id: 538,
              createdDate: new Date(),
              title: 'Multi-channelled',
              description: 'Distinctio rem molestiae recusandae voluptatem nobis quia aut. Asperiores excepturi optio. Ut laboriosam quod dolor error maiores reprehenderit iste. Maxime nam necessitatibus quia exercitationem fuga et. Sequi voluptatem aquasi repudiandae recusandae consequatur voluptatem. Id soluta nemo reiciendis blanditiis eum voluptatem ut pariatur velit.',
              status: Status.locked,
              winnedXP: 50
            }
          ]
        }
      ]
    };

    return game;
  }
});
