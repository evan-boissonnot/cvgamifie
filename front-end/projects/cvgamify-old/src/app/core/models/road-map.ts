import { MapOption, Map } from "@dev-to-be-curious/rand-svg-path-generator/dist"
import { Game } from "./game";

/**
 * One RoadMap model, with quests
 */
export interface RoadMap {
    /**
     * Current game for the roadmap
     */
    game: Game;

    /**
     * Current Map generated by the rand-svg-path-generator
     */
    map: Map;

    /**
     * RoadMap settings for the rand-svg-path-generator
     */
    settings: MapOption;
}
