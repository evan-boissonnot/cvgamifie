import { Status } from "./status";

/**
 * One goal of one quest, from a Game
 */
export interface Goal {
    /** 
     * Id of the goal 
     * */
    id: number;

    /**
     * Date of the creation of the goal
     */
    createdDate: Date;

    /** 
     * Title of the goal
     **/
    title: string;

    /**
     *  Description of the goal 
     **/
    description: string;

    /**
     * Current state of the goal
     */
    status: Status;

    /**
     * Value of the winned XP
     */
    winnedXP: number;
}
