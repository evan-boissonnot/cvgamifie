import {Goal} from './goal';
import { Status } from './status';


/**
 * One quest of one game
 */
export interface Quest {
    /** 
     * Id of the quest 
     * */
    id: number;

    /**
     * Date of creation of the quest
     */
    createdDate: Date;

    /** 
     * Title of the quest
     **/
    title: string;

    /**
     *  Description of the quest 
     **/
    description: string;

    /**
     * List of goals
     */
    goals: Goal[];

    /**
     * Current state of the quest
     */
    status: Status;
}
