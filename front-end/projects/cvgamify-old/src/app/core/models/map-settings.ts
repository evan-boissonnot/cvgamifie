/**
* RoadMap settings for the rand-svg-path-generator
*/

const MapSettings = {
    width: 900,
    height: 550,
    margin: {
      top: 20,
      right: 90,
      bottom: 30,
      left: 90
    },
    delta_y: 150,
    checkpoints: {
      radius: 15
    },
    apiUrl: '', // -> a mettre dans environnement ;)
    data: [],
    successCallback: null,
    errorCallback: null
  };

export default MapSettings;