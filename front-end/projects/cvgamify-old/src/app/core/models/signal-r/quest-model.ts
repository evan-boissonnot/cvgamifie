import { GoalModel } from "./goal-model";

/**
 * One quest of one game
 */
export abstract class QuestModel {
    /** 
     * Id of the quest 
     * */
    id: number;

    /**
     * Date of creation of the quest
     */
    createdDate: Date;

    /** 
     * Title of the quest
     **/
    title: string;

    /**
     *  Description of the quest 
     **/
    description: string;

    /**
     * List of goals
     */
    goals: GoalModel[];

    /**
     * Current state of the quest
     */
    status: string;
}
