import { Status } from '../status';

/**
 * One goal of one quest, from a Game
 */
export abstract class GoalModel {
    /** 
     * Id of the goal 
     * */
    id: number;

    /**
     * Date of the creation of the goal
     */
    createdDate: Date;

    /** 
     * Title of the goal
     **/
    title: string;

    /**
     *  Description of the goal 
     **/
    description: string;

    /**
     * Current state of the goal
     */
    status: string;
}
