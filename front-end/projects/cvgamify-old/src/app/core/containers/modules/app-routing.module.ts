import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from '../components/home/home.component';

const routes: Routes = [
  {
    path: 'student', loadChildren: () => import('../../../features/students/student/student.module').then(m => m.StudentModule)
  },
  {
    path: 'trainer', loadChildren: () => import('../../../features/trainers/trainer.module').then(m => m.TrainerModule)
  },
  {
    path: '', component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
