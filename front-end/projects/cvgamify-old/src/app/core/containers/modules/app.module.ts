import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ChartsModule } from 'ng2-charts';
import { HttpSharedModule } from '../../../shared/modules/http-shared-module/http-shared.module';
import { LoggerService } from '../../../shared/services/logger/logger.service';
import { AppComponent } from '../components/app/app.component';
import { HomeComponent } from '../components/home/home.component';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    HttpSharedModule,
    HttpClientModule,
    AppRoutingModule
  ],providers: [
    LoggerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
