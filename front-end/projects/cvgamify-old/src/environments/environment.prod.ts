export const environment = {
  production: true,
  apis: {
    games: {
      url: 'https://localhost:44378/api/game',
    },
    trainingSessions: {
      url: 'https://localhost:44378/api/trainingSession'
    },
    hubs: {
      student: {
        url: 'https://localhost:44378/hub/student'
      },
      trainer: {
        url: 'https://localhost:44378/hub/trainer'
      }
    }
  }
};
