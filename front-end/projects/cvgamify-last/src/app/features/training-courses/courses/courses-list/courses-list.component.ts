import { Component, OnInit, ViewChild } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { CoursesService } from './service/courses.service';
import { HttpClient } from '@angular/common/http';


import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { TrainingCourse } from 'projects/cvgamify-last/src/app/core/models/training-courses/training-course';


@Component({
  selector: 'cvgamify-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.less']
})
export class CoursesListComponent implements OnInit {

  course: Array<TrainingCourse>;
  loading: boolean = true;
  isExisting: boolean = true;

  @ViewChild('coursesList') table: Table;

  constructor(private coursesService: CoursesService, private primengConfig: PrimeNGConfig, private _httpClient: HttpClient) { }

  ngOnInit(): void {
    const callback = (result: TrainingCourse[]) => {
      this.course = result;
      this.loading = false;
    };

    const prepareQuery$ = this.coursesService.getAll().pipe(takeWhile(() => this.isExisting));
    const executeQuery = prepareQuery$.subscribe(callback);
  }

  ngOnDestroy() {
    this.isExisting = false;
  }


}
