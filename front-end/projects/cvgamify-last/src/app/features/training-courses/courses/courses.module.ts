import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesListComponent } from './courses-list/courses-list.component';
import { CoursesRoutingModule } from './courses-routing.module';

import { TableModule } from 'primeng/table';
import { SliderModule } from 'primeng/slider';
import { DialogModule } from 'primeng/dialog';
import { ContextMenuModule } from 'primeng/contextmenu';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { EditorModule } from 'primeng/editor';

import { NewCourseComponent } from './new-course/new-course.component';
import { SharedImportsModule } from '../../../shared/module/shared.module';
import { DataInputComponent } from '../../../shared/ui/components/data-input/data-input.component';
import { DataEditorComponent } from '../../../shared/ui/components/data-editor/data-editor.component';







@NgModule({
  declarations: [
    CoursesListComponent,
    NewCourseComponent,
    DataInputComponent,
    DataEditorComponent
  ],
  imports: [
    CommonModule,
    SharedImportsModule,
    CoursesRoutingModule,
    TableModule,
    SliderModule,
    DialogModule,
		ContextMenuModule,
		DropdownModule,
		ButtonModule,
    InputTextModule,
    EditorModule
  ],
  exports: [
    NewCourseComponent
  ]
})
export class CoursesModule { }
