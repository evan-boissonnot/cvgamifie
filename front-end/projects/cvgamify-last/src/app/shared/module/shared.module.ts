import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { AccordionModule } from 'primeng/accordion';
import { ToggleButtonModule } from 'primeng/togglebutton';


@NgModule({
  declarations: [],
  imports: [
    InputTextModule,
    ButtonModule,
    AccordionModule,
    ToggleButtonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    InputTextModule,
    ButtonModule,
    AccordionModule,
    ToggleButtonModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class SharedImportsModule { }
