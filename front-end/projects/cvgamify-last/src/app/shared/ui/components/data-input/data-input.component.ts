import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'cvgamify-data-input',
  templateUrl: './data-input.component.html',
  styleUrls: ['./data-input.component.less']
})
export class DataInputComponent implements OnInit {

  @Input() formName: FormGroup
  @Input('inputInfo') public inputInfo: string;
  @Input('inputLabel') public inputLabel: string;
  @Input('inputIsInvalidAndTouched') public inputIsInvalidAndTouched: boolean;

  constructor(private fb: FormBuilder) {

   }

  ngOnInit(): void {

  }

  

}
