import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';



@Component({
  selector: 'cvgamify-data-editor',
  templateUrl: './data-editor.component.html',
  styleUrls: ['./data-editor.component.less']
})
export class DataEditorComponent implements OnInit {

  @Input() formName: FormGroup;
  @Input('editorInfo') public editorInfo: string;
  @Input('placeholder') public placeholder: string;
  @Input('inputIsInvalidAndTouched') public inputIsInvalidAndTouched: boolean;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

}
