import { Status } from '../enums/status';

/**
 * Training course : it's a template to be created by the trainer
 */
export class TrainingCourse {
  constructor(
    //#region Properties
    public id: number,

    public creationDate: Date,

    public activationDate: Date,

    public activeSessionsNumber: number,

    public status: Status,

    public title: string,

    public subtitle: string,

    public description: string
    //#endregion
  ) {}
}
