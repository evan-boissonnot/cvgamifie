/**
 * Status of one item 
 */
export enum Status {
    active = 100,
    pending = 0,
    inactive = -100
}
