﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TrainingCourses.API.Application.Models;
using TrainingCourses.Domain.TrainingCourseAggregate;

namespace TrainingCourses.API.Application.Queries.Handlers
{
    public class GetTrainingCourseDetailHandler : IRequestHandler<GetTrainingCourseDetailQuery, Models.TrainingCourse>
    {
        #region Fields
        private ITrainingCourseRepository _repository = null;
        private IMapper _mapper = null;
        #endregion

        #region Constructors
        public GetTrainingCourseDetailHandler(ITrainingCourseRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this._mapper = mapper;
        }
        #endregion

        #region Public methods
        public async Task<Models.TrainingCourse> Handle(GetTrainingCourseDetailQuery request, CancellationToken cancellationToken)
        {
            Models.TrainingCourse result = null;
            var item = await this._repository.GetAsync(request.Id);

            if (item != null)
            {
                result = this._mapper.Map<Models.TrainingCourse>(item);
            }

            return result;
        }
        #endregion
    }
}
