﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TrainingCourses.API.Application;
using TrainingCourses.API.Application.Models;

namespace TrainingCourses.Application.Queries.Handlers
{
    public class GetAllTrainingCourseFromOneFormerHandler : IRequestHandler<GetTrainingCourseListFromFormerQuery, IList<TrainingCourse>>
    {
        #region Fields
        private Domain.TrainingCourseAggregate.ITrainingCourseRepository _repository = null;
        private IMapper _mapper = null;
        #endregion

        #region Constructors
        public GetAllTrainingCourseFromOneFormerHandler(Domain.TrainingCourseAggregate.ITrainingCourseRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this._mapper = mapper;
        }
        #endregion

        #region Public methods
        public async Task<IList<TrainingCourse>> Handle(GetTrainingCourseListFromFormerQuery request, CancellationToken cancellationToken)
        {
            IList<TrainingCourse> result = new List<TrainingCourse>();

            var trainingCourses = await this._repository.GetAllAsync(request.FormerId);

            foreach (var item in trainingCourses)
            {
                result.Add(this._mapper.Map<TrainingCourse>(item));
            }

            return result;
        }
        #endregion
    }
}
