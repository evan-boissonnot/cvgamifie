﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TrainingCourses.API.Application.Models;

namespace TrainingCourses.Infrastructure.Mappers.Profiles
{
    /// <summary>
    /// Autompapper profile
    /// </summary>
    public class DefaultProfile : Profile
    {
        #region Constructors
        public DefaultProfile()
        {
            this.CreateMap<Domain.TrainingCourseAggregate.TrainingCourse, TrainingCourse>();
            this.CreateMap<TrainingCourse, Domain.TrainingCourseAggregate.TrainingCourse>();

            this.CreateMap<Domain.TrainingCourseAggregate.Section, Section>();
            this.CreateMap<Section, Domain.TrainingCourseAggregate.Section>();

            this.CreateMap<Domain.TrainingCourseAggregate.Session, Session>();
            this.CreateMap<Session, Domain.TrainingCourseAggregate.Session>();
        }
        #endregion
    }
}
