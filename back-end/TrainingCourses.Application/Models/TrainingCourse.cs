﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingCourses.API.Application.Models
{
    /// <summary>
    /// Default DTO to return
    /// </summary>
    public class TrainingCourse: BaseDto
    {
        #region Properties
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Description { get; set; }
        public int OwnerId { get; set; }

        public IList<Section> Sections { get; set; }
        #endregion
    }
}
