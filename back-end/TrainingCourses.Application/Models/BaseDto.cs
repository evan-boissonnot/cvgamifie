﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingCourses.API.Application.Models
{
    /// <summary>
    /// You must inherit from this class to create a new dto
    /// </summary>
    public abstract class BaseDto
    {
        #region Properties
        public int Id { get; set; }
        #endregion
    }
}
