﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingCourses.API.Application.Models
{
    public class Former
    {
        #region Properties
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        #endregion
    }
}
