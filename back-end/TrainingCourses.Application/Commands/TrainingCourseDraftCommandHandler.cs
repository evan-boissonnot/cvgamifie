﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TrainingCourses.API.Application.Models;
using TrainingCourses.Domain.TrainingCourseAggregate;
using TrainingCourse = TrainingCourses.API.Application.Models.TrainingCourse;

namespace TrainingCourses.API.Application.Commands
{
    public class TrainingCourseDraftCommandHandler : IRequestHandler<TrainingCourseDraftCommand, TrainingCourse>
    {
        #region Fields
        private ITrainingCourseRepository _repository = null;
        private IMapper _mapper = null;
        #endregion

        #region Constructors
        public TrainingCourseDraftCommandHandler(ITrainingCourseRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this._mapper = mapper;
        }
        #endregion

        #region Public methods
        public async Task<TrainingCourse> Handle(TrainingCourseDraftCommand request, CancellationToken cancellationToken)
        {
            Domain.TrainingCourseAggregate.TrainingCourse domainItem = Domain.TrainingCourseAggregate.TrainingCourse.NewDraft(request.OwnerId, request.Title, request.Subtitle, request.Description);

            this._repository.Add(domainItem);
            await this._repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            return this._mapper.Map<TrainingCourse>(domainItem);
        }
        #endregion
    }
}
