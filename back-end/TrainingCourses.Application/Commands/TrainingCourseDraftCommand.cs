﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using TrainingCourses.API.Application.Models;

namespace TrainingCourses.API.Application.Commands
{
    /// <summary>
    /// Commands to create a new training course
    /// </summary>
    [DataContract]
    public class TrainingCourseDraftCommand : IRequest<TrainingCourse>
    {
        #region Fields
        [DataMember]
        private readonly List<Section> _sectionItems;
        #endregion

        #region Constructors
        public TrainingCourseDraftCommand()
        {
            this._sectionItems = new List<Section>();
        }

        public TrainingCourseDraftCommand(string title, string subtitle, string description, int ownerId) : this()
        {
            this.Title = title;
            this.Subtitle = subtitle;
            this.Description = description;
            this.OwnerId = ownerId;
        }

        public TrainingCourseDraftCommand(string title, string subtitle, string description, int ownerId, params Section[] sections) : this(title, subtitle, description, ownerId)
        {
            this.Title = title;
            this.Subtitle = subtitle;
            this.Description = description;
            this._sectionItems.AddRange(sections);
        }
        #endregion

        #region Properties
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Subtitle { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int OwnerId { get; set; }

        [DataMember]
        public IEnumerable<Section> SectionItems { get; set; }
        #endregion
    }
}
