﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingCourses.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FormerController : BaseApiController
    {
        #region Fields

        #endregion

        #region Constructors
        public FormerController(IMediator mediator) : base(mediator)
        {

        }
        #endregion
    }
}
