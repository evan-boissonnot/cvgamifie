﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingCourses.API.Controllers
{
    /// <summary>
    /// You must inherits from this class to create a API controller
    /// </summary>
    public abstract class BaseApiController : ControllerBase
    {
        #region Fields
        private IMediator _mediator = null;
        #endregion

        #region Constructors
        public BaseApiController(IMediator mediator)
        {
            this._mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #endregion

        #region Properties
        /// <summary>
        /// Access to the mediator to manage queries and commands
        /// </summary>
        protected IMediator Mediator => this._mediator;
        #endregion
    }
}
