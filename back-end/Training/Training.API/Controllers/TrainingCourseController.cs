﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TrainingCourses.API.Application.Commands;
using TrainingCourses.API.Application.Models;
using TrainingCourses.Application.Queries;

namespace TrainingCourses.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TrainingCourseController : BaseApiController
    {
        #region Constructors
        public TrainingCourseController(IMediator mediator): base(mediator)
        {
            
        }
        #endregion

        #region Public methods
        [HttpPost]
        [Route("draft")]
        public async Task<IActionResult> CreateTrainingCourseAsDraftAsync([FromBody] TrainingCourseDraftCommand command)
        {
            IActionResult result = null;
            TrainingCourse model = null;

            model = await this.Mediator.Send(command);
            result = this.Ok(model);

            if(model == null)
            {
                result = this.BadRequest();
            }

            return result;
        }

        [Route("{trainingCourseId:int}")]
        [HttpGet]
        [ProducesResponseType(typeof(TrainingCourse), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetTrainingCourseAsync(int trainingCourseId)
        {
            IActionResult result = null;
            TrainingCourse model = null;

            model = await this.Mediator.Send(new Application.Queries.GetTrainingCourseDetailQuery(trainingCourseId));
            result = Ok(model);

            if (model == null)
            {
                result = NotFound();
            }

            return result;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IList<TrainingCourse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetTrainingCoursesFromFormerAsync([FromQuery] int formerId)
        {
            IActionResult result = NotFound();

            var data = await this.Mediator.Send(new GetTrainingCourseListFromFormerQuery(formerId));
            if (data != null)
            {
                result = Ok(data);
            }

            return result;
        }
        #endregion
    }
}
