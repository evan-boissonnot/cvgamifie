﻿using CVGamify.Framework;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace TrainingCourses.Infrastructure.ExtensionMethods
{
    /// <summary>
    /// Allows you to publish last update through aggregates
    /// // https://devblogs.microsoft.com/cesardelatorre/using-domain-events-within-a-net-core-microservice/
    /// </summary>
    public static class MediatRExtensionMethods
    {
        #region Pubblic methods
        /// <summary>
        /// Dipatch all domain events before, or after savind data in context
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="ctx"></param>
        /// <returns></returns>
        public static async Task DispatchDomainEventsAsync(this IMediator mediator, TrainingCourseDbContext ctx)
        {
            var domainEntities = ctx.ChangeTracker
                .Entries<Entity>()
                .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any());

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .ToList();

            domainEntities.ToList()
                .ForEach(entity => entity.Entity.ClearDomainEvents());

            var tasks = domainEvents
                .Select(async (domainEvent) => {
                    await mediator.Publish(domainEvent);
                });

            await Task.WhenAll(tasks);
        }
        #endregion
    }
}
