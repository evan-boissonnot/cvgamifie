﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TrainingCourses.Domain.TrainingCourseAggregate;

namespace TrainingCourses.Infrastructure.EntityConfigurations
{
    public class TrainingCourseEntityConfiguration : IEntityTypeConfiguration<TrainingCourse>
    {
        #region Public methods
        public void Configure(EntityTypeBuilder<TrainingCourse> builder)
        {
            builder.ToTable("TrainingCourse");

            builder.HasKey(o => o.Id);
            builder.Ignore(b => b.DomainEvents);

            builder.HasOne(o => o.Status)
                .WithMany()
                .HasForeignKey("StatusId");

            builder.HasOne(o => o.Theme)
                .WithMany()
                .HasForeignKey("ThemeId");
        }
        #endregion
    }
}
