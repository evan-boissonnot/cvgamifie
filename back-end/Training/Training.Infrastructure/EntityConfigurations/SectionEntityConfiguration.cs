﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TrainingCourses.Domain.TrainingCourseAggregate;

namespace TrainingCourses.Infrastructure.EntityConfigurations
{
    public class SectionEntityConfiguration : IEntityTypeConfiguration<Section>
    {
        #region Public methods
        public void Configure(EntityTypeBuilder<Section> builder)
        {
            builder.ToTable("Section");

            builder.HasKey(o => o.Id);
            builder.Ignore(b => b.DomainEvents);

            builder.Property<int>("TrainingCourseId")
                   .IsRequired();
        }
        #endregion
    }
}
