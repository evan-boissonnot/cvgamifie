﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TrainingCourses.Domain.TrainingCourseAggregate;

namespace TrainingCourses.Infrastructure.EntityConfigurations
{
    public class TrainingCourseStatusEntityConfiguration : IEntityTypeConfiguration<TrainingCourseStatus>
    {
        #region Public methods
        public void Configure(EntityTypeBuilder<TrainingCourseStatus> builder)
        {
            builder.ToTable("TrainingCourseStatus");
            builder.HasKey(item => item.Id);
            
            builder.Property(item => item.Id)
                   .HasDefaultValue(1)
                   .ValueGeneratedNever()
                   .IsRequired();

            builder.Property(item => item.Name)
                   .HasMaxLength(200)
                   .IsRequired();
        }
        #endregion
    }
}
