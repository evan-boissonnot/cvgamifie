﻿using CVGamify.Framework;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using TrainingCourses.Domain;
using TrainingCourses.Domain.TrainingCourseAggregate;
using TrainingCourses.Infrastructure.EntityConfigurations;
using TrainingCourses.Infrastructure.ExtensionMethods;

namespace TrainingCourses.Infrastructure
{
    public class TrainingCourseDbContext : DbContext, IUnitOfWork
    {
        #region Fields
        private IMediator _mediator = null;
        #endregion

        #region Constructors
        public TrainingCourseDbContext(DbContextOptions<TrainingCourseDbContext> options) : base(options) {}

        public TrainingCourseDbContext(DbContextOptions<TrainingCourseDbContext> options, IMediator mediator) : this(options) 
        {
            this._mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #endregion

        #region Internal methods
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TrainingCourseEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TrainingCourseStatusEntityConfiguration());
            modelBuilder.ApplyConfiguration(new SectionEntityConfiguration());
            modelBuilder.ApplyConfiguration(new SessionEntityConfiguration());
            modelBuilder.ApplyConfiguration(new ThemeEntityConfiguration());
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            await this._mediator.DispatchDomainEventsAsync(this);
            int result = await base.SaveChangesAsync();

            return result > 0;
        }
        #endregion

        #region Properties
        public DbSet<TrainingCourse> TrainingCourses { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<TrainingCourseStatus> TrainingCourseStatus { get; set; }
        #endregion
    }
}
