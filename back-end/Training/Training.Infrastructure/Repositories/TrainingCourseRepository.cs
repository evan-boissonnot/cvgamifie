﻿using CVGamify.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TrainingCourses.Domain.TrainingCourseAggregate;
using System.Linq;

namespace TrainingCourses.Infrastructure.Repositories
{
    public class TrainingCourseRepository : ITrainingCourseRepository
    {
        #region Fields
        private readonly TrainingCourseDbContext _context = null;
        #endregion

        #region Constructors
        public TrainingCourseRepository(TrainingCourseDbContext context)
        {
            this._context = context ?? throw new ArgumentNullException(nameof(context));
        }
        #endregion

        #region Public methods
        public TrainingCourse Add(TrainingCourse trainingCourse)
        {
            return this._context.TrainingCourses.Add(trainingCourse).Entity;
        }

        public async Task<TrainingCourse> GetAsync(int id)
        {
            TrainingCourse trainingCourse = await this._context.TrainingCourses.FindAsync(id);

            if (trainingCourse != null)
            {
                await this._context.Entry(trainingCourse).Collection(collection => collection.Sections).LoadAsync();

                foreach (var section in trainingCourse.Sections)
                {
                    await this._context.Entry(section).Collection(collection => collection.Sessions).LoadAsync();
                }

                await this._context.Entry(trainingCourse).Reference(item => item.Status).LoadAsync();
            }

            return trainingCourse;
        }

        public void Update(TrainingCourse course)
        {
            this._context.Entry(course).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        }

        public Task<IList<TrainingCourse>> GetAllAsync(int formerId)
        {
            var result = from item in this._context.TrainingCourses
                         where item.OwnerId == formerId
                         select item;

            return Task.FromResult<IList<TrainingCourse>>(result.ToList());
        }
        #endregion

        #region Properties
        public IUnitOfWork UnitOfWork => this._context;
        #endregion
    }
}
