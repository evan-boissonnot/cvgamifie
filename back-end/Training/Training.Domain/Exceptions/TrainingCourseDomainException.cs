﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace TrainingCourses.Domain.Exceptions
{
    /// <summary>
    /// Use it when domain is not valid
    /// </summary>
    public class TrainingCourseDomainException : Exception
    {
        #region Public methods
        public TrainingCourseDomainException()
        {
        }

        public TrainingCourseDomainException(string message) : base(message)
        {
        }

        public TrainingCourseDomainException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected TrainingCourseDomainException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
        #endregion
    }
}
