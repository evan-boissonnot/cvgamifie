﻿using CVGamify.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingCourses.Domain.TrainingCourseAggregate
{
    /// <summary>
    /// Section is a group of session
    /// Section can only exists if trainingcourse exists. Training can have multiple sections.
    /// Section can have multiple training sessions
    /// </summary>
    public class Section: Entity
    {
        #region Fields
        private readonly List<Session> _sessionsItems;
        #endregion

        #region Constructors
        public Section()
        {
            this._sessionsItems = new List<Session>();
        }

        public Section(int sortId, string title, string description): this()
        {
            this.SortId = sortId;
            this.Title = title;
            this.Description = description;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Adds new session inside the training course
        /// </summary>
        /// <param name="title"></param>
        /// <param name="description"></param>
        public void AddNewSession(int id, string title, string description)
        {
            Session session = new Session(id, title, description, this);

            this._sessionsItems.Add(session);
            this.AddDomainEvent(new Events.TrainingCourseSessionAdded(id, title, description, this));
        }
        #endregion

        #region Properties
        public string Title { get; set; }
        public string Description { get; set; }
        /// <summary>
        /// SortId is section own position into the training course sections list
        /// 1 if it's the first section; 3 if it's the third session into the list
        /// </summary>
        public int SortId { get; set; }
        public TrainingCourse TrainingCourse { get; set; }
        public int TrainingCourseId { get; set; }
        public IReadOnlyCollection<Session> Sessions { get; set; }
        #endregion
    }
}
