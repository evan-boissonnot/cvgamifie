﻿using CVGamify.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TrainingCourses.Domain.Exceptions;

namespace TrainingCourses.Domain.TrainingCourseAggregate
{
    /// <summary>
    /// States of a training course (created to published)
    /// </summary>
    public class TrainingCourseStatus : Enumeration
    {
        #region Values
        public static TrainingCourseStatus Draft = new TrainingCourseStatus(1, nameof(Draft).ToLowerInvariant());
        public static TrainingCourseStatus Active = new TrainingCourseStatus(2, nameof(Active).ToLowerInvariant());
        public static TrainingCourseStatus Inactive = new TrainingCourseStatus(-1, nameof(Inactive).ToLowerInvariant());
        #endregion

        #region Constructors
        public TrainingCourseStatus(int id, string name) : base(id, name) {}
        #endregion

        #region Public methods
        /// <summary>
        /// Whole status of the enumeration
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<TrainingCourseStatus> List() => new[] { Draft, Active, Inactive };

        /// <summary>
        /// Gets one instance from its name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static TrainingCourseStatus FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
            {
                throw new TrainingCourseDomainException($"Possible values for OrderStatus: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

        /// <summary>
        /// Gets one instance from its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static TrainingCourseStatus From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
            {
                throw new TrainingCourseDomainException($"Possible values for OrderStatus: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }
        #endregion
    }
}
