﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingCourses.Domain.TrainingCourseAggregate.Events
{
    /// <summary>
    /// Raised when a new session is added inside the training course
    /// </summary>
    public class TrainingCourseSessionAdded : INotification
    {
        #region Constructors
        public TrainingCourseSessionAdded(int id, string title, string description, Section owner)
        {
            this.Id = id;
            this.Title = title;
            this.Description = description;
            this.Owner = owner;
        }
        #endregion

        #region Properties
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Section Owner { get; set; }
        #endregion
    }
}
