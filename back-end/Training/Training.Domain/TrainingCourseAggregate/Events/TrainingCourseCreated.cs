﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingCourses.Domain.TrainingCourseAggregate.Events
{
    /// <summary>
    /// Instance to define when a new training course has been created
    /// </summary>
    public class TrainingCourseCreated : INotification
    {
        #region Constructors
        public TrainingCourseCreated(int id, string title, string description, int ownerId)
        {
            this.Id = id;
            this.Title = title;
            this.Description = description;
            this.OwnerId = ownerId;
        }
        #endregion

        #region Properties
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Theme Theme { get; set; }
        public int ThemeId { get; set; }
        #endregion
    }
}
