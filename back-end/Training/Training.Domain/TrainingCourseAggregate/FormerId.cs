﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingCourses.Domain.TrainingCourseAggregate
{
    /// <summary>
    /// Encapsulates the Id of the former
    /// Former is not in this context, so we link to Former by its id
    /// </summary>
    public class FormerId
    {
        #region Fields
        private readonly int _value;
        #endregion

        #region Constructors
        public FormerId(int value)
        {
            this._value = value;
        }
        #endregion

        #region Properties
        public int Value { get => this._value; }
        #endregion
    }
}
