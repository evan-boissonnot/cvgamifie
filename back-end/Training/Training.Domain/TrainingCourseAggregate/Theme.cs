﻿using CVGamify.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingCourses.Domain.TrainingCourseAggregate
{    
    /// <summary>
     /// Theme define the theme of a training course.
     /// It will define a section diffrently like ( if theme is aventure -> section will be an aventure; military -> Section will be an mission
     /// </summary>
    public class Theme: Entity
    {
        #region Properties
        public string Name { get; set; }
        #endregion
    }
}
