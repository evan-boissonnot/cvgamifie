﻿using CVGamify.Framework;

namespace TrainingCourses.Domain.TrainingCourseAggregate
{
    /// <summary>
    /// Section can only exists if section exists. A section can have multiple sessions.
    /// </summary>
    public class Session : Entity
    {
        #region Constructors
        public Session() { }

        public Session(int id, string title, string description, Section owner): this()
        {
            this.Id = id;
            this.Title = title;
            this.Description = description;
            this.Section = owner;
        }
        #endregion

        #region Properties
        public string Title { get; set; }
        public string Description { get; set; }
        /// <summary>
        /// SortId is session own position into the section session list
        /// 1 if it's the first session into the section; 3 if it's the third session into the section
        /// </summary>
        public int SortId { get; set; }
        public Section Section { get; set; }
        public int SectionId { get; set; }
        #endregion
    }
}
