﻿using CVGamify.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TrainingCourses.Domain.TrainingCourseAggregate
{
    /// <summary>
    /// Contract from the domain layer to access to training course aggregate
    /// </summary>
    public interface ITrainingCourseRepository : IRepository<TrainingCourse>
    {
        /// <summary>
        /// Creates a new training course and save it
        /// </summary>
        /// <param name="trainingCourse"></param>
        /// <returns></returns>
        TrainingCourse Add(TrainingCourse trainingCourse);

        /// <summary>
        /// Updates one training course
        /// </summary>
        /// <param name="course"></param>
        void Update(TrainingCourse course);

        /// <summary>
        /// Gets one training course
        /// </summary>
        /// <param name="id">Id of the entity</param>
        /// <returns></returns>
        Task<TrainingCourse> GetAsync(int id);

        /// <summary>
        /// Gets all training course from one former
        /// </summary>
        /// <returns></returns>
        Task<IList<TrainingCourse>> GetAllAsync(int formerId);
    }
}
