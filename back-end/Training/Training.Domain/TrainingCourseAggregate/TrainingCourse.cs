﻿using CVGamify.Framework;
using CVGamify.Framework.Exceptions;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TrainingCourses.Domain.TrainingCourseAggregate
{
    /// <summary>
    /// Trainingcourse is generated from a Former.
    /// A training course contains multiple sections 
    /// The former can define a theme - it will have a visual effect ( Aventure, Military, Travel, ....)
    /// </summary>
    public class TrainingCourse : Entity, IAggregateRoot
    {
        #region Fields
        private readonly List<Section> _sectionsItems;
        private TrainingCourseStatus _status;
        #endregion

        #region Constructors
        public TrainingCourse() 
        {
            this._sectionsItems = new List<Section>();
        }

        public TrainingCourse(int id, int ownerId): this()
        {
            this.Id = id;
            this.OwnerId = ownerId;
        }

        public TrainingCourse(int id, string title, string subtitle, string description, int ownerId): this()
        {
            this.Id = id;
            this.Title = title;
            this.Subtitle = subtitle;
            this.Description = description;
            this.OwnerId = ownerId;

            this.ControlData();
            this.AddDomainEvent(new Events.TrainingCourseCreated(this.Id, this.Title, this.Description, this.OwnerId));
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Adds new session inside the training course
        /// </summary>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="sessions">You can add some sessions to be added in the new section</param>
        public void AddSection(int id, string title, string description, params Session[] sessions)
        {
            Section section = new Section(id, title, description);
            this._sectionsItems.Add(section);

            this.AddDomainEvent(new Events.TrainingCourseSectionAdded(id, title, description, this));

            foreach (var item in sessions)
            {
                this.AddSession(item.Id, item.Title, item.Description, section);
            }
        }

        /// <summary>
        /// Adds new session inside the training course
        /// </summary>
        /// <param name="title"></param>
        /// <param name="description"></param>
        public void AddSession(int id, string title, string description, Section owner)
        {
            Section realSection = this._sectionsItems.First(item => item.Id == owner.Id);
            realSection.AddNewSession(id, title, description);
        }

        /// <summary>
        /// Publishes the training course
        /// </summary>
        public void Publish()
        {
            this.AddDomainEvent(new Events.TrainingCoursePublished(this));
            this.StatusId = TrainingCourseStatus.Active.Id;
        }

        /// <summary>
        /// Returns new instance of order, with draft status
        /// </summary>
        /// <returns></returns>
        public static TrainingCourse NewDraft(int ownerId, string title, string subtitle, string description)
        {
            TrainingCourse item = new TrainingCourse(0, title, subtitle, description, ownerId);

            item.StatusId = TrainingCourseStatus.Draft.Id;

            return item;
        }
        #endregion

        #region Internal methods
        private void ControlData()
        {
            if (string.IsNullOrEmpty(this.Title))
            {
                throw new ArgumentNullException("title");
            }

            if (string.IsNullOrEmpty(this.Description))
            {
                throw new ArgumentNullException("description");
            }

            if (this.OwnerId <= 0)
            {
                throw new ArgumentNullException("ownerId");
            }
        }
        #endregion

        #region Properties
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Description { get; set; }
        public Theme Theme { get; set; }
        public int OwnerId { get; set; }
        public TrainingCourseStatus Status
        {
            get => this._status;
            private set
            {
                this._status = value;
            }
        }

        public int StatusId { get; private set; }

        public IReadOnlyCollection<Section> Sections { get; set; }
        #endregion
    }
}
