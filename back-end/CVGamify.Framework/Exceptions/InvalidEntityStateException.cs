﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CVGamify.Framework.Exceptions
{
    /// <summary>
    /// Uses it when a state of one entity is not valid (some values inside the entity are null, or empty, or ...)
    /// </summary>
    public class InvalidEntityStateException : Exception
    {
        #region Constructors
        public InvalidEntityStateException(object entity, string message)
            : base($"Entity {entity.GetType().Name} state change rejected, {message}")
        {
        }
        #endregion
    }
}
