﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CVGamify.Framework
{
    /// <summary>
    /// DDD repository, it ont ly use aggregate root entity
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : IAggregateRoot
    {
        /// <summary>
        /// Access to save actions
        /// </summary>
        IUnitOfWork UnitOfWork { get; }
    }
}
