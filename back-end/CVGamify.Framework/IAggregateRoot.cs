﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CVGamify.Framework
{
    /// <summary>
    /// Uses it to define an entity to be an aggregate root
    /// </summary>
    public interface IAggregateRoot
    {
    }
}
